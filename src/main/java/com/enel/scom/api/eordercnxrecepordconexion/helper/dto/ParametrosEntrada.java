package com.enel.scom.api.eordercnxrecepordconexion.helper.dto;

import java.util.Optional;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ParametrosEntrada {
	private String nroODT;
	private String pathPlano;
	private Optional<String> origen;
}

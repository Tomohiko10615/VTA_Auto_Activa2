package com.enel.scom.api.eordercnxrecepordconexion.helper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.enel.scom.api.eordercnxrecepordconexion.helper.dto.ParametrosEntrada;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProcesadorEntrada {
	
	private ProcesadorEntrada() {
		super();
	}
	
	private static List<String> args;
	private static String origen = null;

	private static final List<Integer> NUM_ARGS_VALIDOS = Arrays.asList(3, 4);
	private static final int ERROR_STATUS = 1;
	
	public static ParametrosEntrada procesar(List<String> argsEntrada) {
		args = argsEntrada;
		validar();
		return generarParametrosEntrada();
	}

	private static void validar() {
		int numArgs = args.size();
		if (!NUM_ARGS_VALIDOS.contains(numArgs)) {
			log.error("\n*****NUMERO INVALIDO DE ARGUMENTOS DE ENTRADA*****");
			log.error("\nUsar: <Orden> <Ruta_Origen> <archivo_datos> [<ORIGEN>]");
			System.exit(ERROR_STATUS);
		}
		try {
			Long.parseLong(args.get(0));
		} catch (NumberFormatException e) {
			log.error("\n*****NUMERO DE ODT NO VALIDO*****");
			log.error("\nUsar un valor numerico");
			System.exit(ERROR_STATUS);
		}
		if (numArgs == Collections.max(NUM_ARGS_VALIDOS)) {
			origen = args.get(numArgs - 1);
			if (!origen.equals("EORDER")) {
				log.error("\n*****ORIGEN INVALIDO*****");
				log.error("\nUsar: EORDER");
				System.exit(ERROR_STATUS);
			}
		}
	}
	
	private static ParametrosEntrada generarParametrosEntrada() {
		return ParametrosEntrada.builder()
				.nroODT(args.get(0))
				.pathPlano(args.get(1).concat(args.get(2)))
				.origen(Optional.ofNullable(origen))
				.build();
	}
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.AnexosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.FechaDTO;

public class FechaHoraMapper implements RowMapper<FechaDTO> {
	public static final String SQLPOSTGRESQL_SELECT_FOR_FECHA_HORA_HOY = "SELECT " +
			" TO_CHAR(NOW(), ? ) atributo1, '' atributo2, '' atributo3";
	
	public static final String SQLORACLE_SELECT_FOR_FECHA_DIF="SELECT '' atributo1, TRUNC(TO_DATE(?,'dd/mm/yyyy')) - TRUNC(SYSDATE) atributo2,\r\n"
			+ "	 				 TRUNC(TO_DATE(?,'dd/mm/yyyy')) - TRUNC(TO_DATE(?,'dd/mm/yyyy')) atributo3\r\n"
			+ "	 		   FROM DUAL";
	
	@Override
	public FechaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		FechaDTO fecha= new FechaDTO();		
		String fechaHora = rs.getString("atributo1");
		String dif1 = rs.getString("atributo2");
		String dif2 = rs.getString("atributo3");
		fecha.setFechaHora(fechaHora);
		fecha.setDif1(dif1);
		fecha.setDif2(dif2);
		return fecha;
	}
	
}

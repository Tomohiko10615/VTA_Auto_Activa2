package com.enel.scom.api.eordercnxrecepordconexion.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MedLecUltimaDTO {

	private Long id_lectura;
	private Long id_componente;
	private Long id_medida;
	private Long id_empresa;
	
}

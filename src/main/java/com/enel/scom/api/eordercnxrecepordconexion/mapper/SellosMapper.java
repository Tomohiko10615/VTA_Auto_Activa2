package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.SellosDTO;

public class SellosMapper implements RowMapper<SellosDTO> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_SELLO = "SELECT\r\n"
			+ " unnest(xpath('/item/numeroSello/text()', d.REG_XML))::text AS numeroSello,\r\n"
			+ " unnest(xpath('/item/ubicacionSello/text()', d.REG_XML))::text AS ubicacionSello,\r\n"
			+ " unnest(xpath('/item/serieSello/text()', d.REG_XML))::text AS serieSello,\r\n"
			+ " unnest(xpath('/item/tipoSello/text()', d.REG_XML))::text AS tipoSello,\r\n"
			+ " unnest(xpath('/item/colorSello/text()', d.REG_XML))::text AS colorSello,\r\n"
			+ "	unnest(xpath('/item/estadoSello/text()', d.REG_XML))::text AS estadoSello,\r\n"
			+ " unnest(xpath('/item/accionSello/text()', d.REG_XML))::text AS accionSello\r\n"
			+ "FROM (SELECT  unnest(xpath('/sellos/item', unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/sellos', d.REG_XML)))) AS REG_XML  \r\n"
			+ "                    FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  \r\n"
			+ "                    WHERE d.id_ord_transfer =   ?\r\n"
			+ "                    	AND accion = 'RECEPCION'  \r\n"
			+ "                    	AND nro_evento = ?)d";

	@Override
	public SellosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String numeroSello = rs.getString("numeroSello");
		String ubicacionSello = rs.getString("ubicacionSello");
		String serieSello = rs.getString("serieSello");
		String tipoSello = rs.getString("tipoSello");
		String colorSello = rs.getString("colorSello");
		String estadoSello = rs.getString("estadoSello");
		String accionSello = rs.getString("accionSello");
		return new SellosDTO(numeroSello, ubicacionSello, serieSello, tipoSello, colorSello, estadoSello, accionSello);
	}
	
}

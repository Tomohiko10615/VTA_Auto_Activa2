package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SuministroDTO {

	private Long idRegistro;
	private Long idAlim;
	private Long idSed;
	private Long idLlave;
	private Long idRutaSumin;
}

package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DatosProcesosDTO {

	private String codigoDistribuidora="";
	private String codigoSistemaExternoOrigen="";
	private String codigoTipoTdC="";
	private String codigoExternoTdC="";
	private String codigoInternoTdC="";
	private String codigoResultado="";
	private String codigoCausalResultado="";
	private String codigoNotaCodificada="";
	private String inicioTdC="";
	private String finTdC="";
	private String duracionEjecucion="";
	private String codigoCuadrilla="";
	private String latitudMedidor="";
	private String longitudMedidor="";
	private String latitudEmpalme="";
	private String longitudEmpalme="";
}

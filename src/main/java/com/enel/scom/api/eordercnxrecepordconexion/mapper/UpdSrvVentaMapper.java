package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class UpdSrvVentaMapper {

	public static final String SQLORACLE_UPDATE_FOR_SRVVENTA = "UPDATE"
			+ " vta_srv_venta"
			+ " SET fecha_ejecucion = to_date(?,'DD/MM/YYYY HH24:MI:SS'),"
//			+ " SET fecha_ejecucion = to_date(substr(REPLACE('?','T',' '), 1,19 ),'YYYY-MM-DD HH24:MI:SS'),"
			+ " estado = 'Ejecutado'"
			+ " WHERE id_srv_venta = ?";
	
	public static final String SQLORACLE_UPDATE_FOR_SRVVENTA2 = "UPDATE"
			+ " vta_srv_venta"
			+ "	SET FECHA_HABILITACION = sysdate,"
			+ "	estado = 'Anulado'"
			+ "	WHERE id_ord_venta = ?"
			+ "	AND id_conf_pres_vta = ?"
			+ "	AND estado in ('NoEjecutado')"
			+ "	AND nvl(id_sol_srv_ele,0) <> 0";
	
	public static final String SQLORACLE_UPDATE_FOR_SRVVENTA3 = "UPDATE"
			+ " vta_srv_venta"
			+ " SET fecha_ejecucion = to_date(?,'DD/MM/YYYY HH24:MI:SS'),"
//			+ " SET fecha_ejecucion = to_date(substr(REPLACE('?','T',' '), 1,19 ),'YYYY-MM-DD HH24:MI:SS'),"
			+ " estado = 'Ejecutado'"
			+ " WHERE id_ord_venta = ?"
			+ " AND id_conf_pres_vta = ?"
			+ " AND estado in ('Generado', 'Inspeccionado','NoEjecutado')"
			+ " AND nvl(id_sol_srv_ele,0) = 0";
	
	public static final String SQLORACLE_UPDATE_FOR_SRVVENTA4 = "UPDATE"
			+ " vta_srv_venta"
			+ "	SET FECHA_HABILITACION  = Null,"
			+ "	ESTADO = 'NoEjecutado'"
			+ "	WHERE ID_ORD_VENTA IN (SELECT a.ID_ORDEN FROM ord_orden a "
			+ " where a.nro_orden = ? and id_empresa = 3 and id_tipo_orden = 5)"
			+ "	AND estado ='Anulado'"
			+ "	AND fecha_habilitacion IS NOT NULL";
	
}

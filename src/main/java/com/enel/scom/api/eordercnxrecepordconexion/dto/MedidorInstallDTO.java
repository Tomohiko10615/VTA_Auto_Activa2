package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MedidorInstallDTO {

	private String numeroMedidor;
	private String codMarca;
	private String codModelo;
	private String fecLectTerreno;
	private String novedad;
	private String mACFP;
	private String mACHP;
	private String mACTI;
	private String mDMFP;
	private String mDMHP;
	private String mREAC;
	private String primeraLectura;
	private String factor;
}

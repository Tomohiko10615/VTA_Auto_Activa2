package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class UpdSrvElectricoMapper {

	public static final String SQLORACLE_UPDATE_FOR_SRVELECTRICO = "UPDATE EOR_SRV_ELEC_XY"
			+ " SET LATITUD	= ?,"
			+ "	LONGITUD = ?,"
			+ "	LATITUD_PCR	= ?,"
			+ "	LONGITUD_PCR = ?,"
			+ "	FECHA_ACTUALIZA = SYSDATE"
			+ " WHERE ID_SERVICIO = ?";
	
	public static final String SQLORACLE_UPDATE_FOR_SRVELECTRIC = "UPDATE srv_electrico"
			+ " SET id_estado = 5"
			+ " WHERE id_servicio = :vaut.id_srv_elec";
	
}

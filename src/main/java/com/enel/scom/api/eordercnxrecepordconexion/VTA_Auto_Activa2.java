package com.enel.scom.api.eordercnxrecepordconexion;

import java.sql.Connection;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import com.enel.scom.api.eordercnxrecepordconexion.util.ArchivoUtil;
import com.enel.scom.api.eordercnxrecepordconexion.util.Constante;
import com.enel.scom.api.eordercnxrecepordconexion.util.Log;

import lombok.extern.slf4j.Slf4j;

import com.enel.scom.api.eordercnxrecepordconexion.config.SynergiaDataSourceJDBCConfiguration;
import com.enel.scom.api.eordercnxrecepordconexion.dao.AutoActivaDAO;
import com.enel.scom.api.eordercnxrecepordconexion.dao.ParametrosDAO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.ParametrosDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RegistroOrdenDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.SrvPtoEntregaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.VtaAutoActDTO;
import com.enel.scom.api.eordercnxrecepordconexion.helper.GeneradorPlano;
import com.enel.scom.api.eordercnxrecepordconexion.helper.LectorPlano;
import com.enel.scom.api.eordercnxrecepordconexion.helper.ProcesadorEntrada;
import com.enel.scom.api.eordercnxrecepordconexion.helper.dto.ParametrosEntrada;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.SrvPtoEntregaMapper;
import com.enel.scom.api.eordercnxrecepordconexion.service.OrdRecepConexionService;

@SpringBootApplication
@Slf4j
public class VTA_Auto_Activa2 implements CommandLineRunner {

	@Autowired
	OrdRecepConexionService service;
	
	@Autowired
	private AutoActivaDAO autoActivaDAO;
	@Autowired
	private ParametrosDAO parametrosDAO;
	
	@Autowired
	@Qualifier("jdbcTemplate1")
	private JdbcTemplate jdbcTemplateOracle;
	
	@Autowired
	@Qualifier("scomConn")
	private Connection scomConn;
	
	private static final String EMPRESA = "EDEL";
	private static final String USUARIO = "pe02850937";
	private static final int SUCCESS_CODE = 0;
	
	// Dependencias necesarias para pruebas
	
	// Dependencias necesarias para pruebas
	
	public static void main(String[] args) {
		SpringApplication.run(VTA_Auto_Activa2.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/*
		//Argumentos de prueba JAAG
		 String odt = "77";
		 args = new String[4];
		 args[0] = odt;
		 args[1] = "C:\\temp\\";
		 args[2] = String.format("EOR_CNX_ORD_CONEXION_ACTIVAR_2318855.txt");
		 args[3] = "EORDER"; */
		 //FIN ARGUMENTOS DE PRUEBA
		// testFTP(args); PRUEBAS FTP
		// System.exit(0);
		//testValidarFactor();
		//System.exit(0);
		
		ParametrosEntrada parametrosEntrada = ProcesadorEntrada.procesar(Arrays.asList(args));
		ParametrosDTO parametrosDTO = parametrosDAO.obtenerParametros(EMPRESA, USUARIO);
		GeneradorPlano.generar(parametrosEntrada, parametrosDTO.getFechaActual(), parametrosDTO.getPathLog());
		List<RegistroOrdenDTO> listaRegistroOrden = LectorPlano.obtenerOrdenes(parametrosEntrada.getPathPlano(), parametrosEntrada.getOrigen());
		autoActivaDAO.procesoAutoActiva(listaRegistroOrden, parametrosDTO, parametrosEntrada);
		GeneradorPlano.subirAServidorFTP();
		System.exit(SUCCESS_CODE);
	}
	
	private void testValidarFactor() {
		RegistroOrdenDTO registroOrden = new RegistroOrdenDTO();
		registroOrden.setNumOrden("2247576");
		registroOrden.setFecLecturaInstall("09/05/2023");
		registroOrden.setCodMarcaInstall("SNX");
		registroOrden.setCodModeloInstall("8CA03");
		registroOrden.setNumMedidorInstall("3156");
		registroOrden.setCodFactor("100");
		autoActivaDAO.validarFactor(registroOrden);
	}

	private void testVTAActivaOrden(String... args) {
		ParametrosEntrada parametrosEntrada = ProcesadorEntrada.procesar(Arrays.asList(args));
		ParametrosDTO parametrosDTO = parametrosDAO.obtenerParametros(EMPRESA, USUARIO);
		GeneradorPlano.generar(parametrosEntrada, parametrosDTO.getFechaActual(), parametrosDTO.getPathLog());
		// Mock de datos para pruebas
		VtaAutoActDTO vtaAutoActDTO = new VtaAutoActDTO();
		//vtaAutoActDTO.setIdAutoAct(354343L);
		vtaAutoActDTO.setIdAutoAct(123L);
		String origen = "EORDER";
		// Mock de datos para pruebas
		
		String sqlFuncionServiceActivarOrden = "SELECT vta_activar_orden(?) as vta_activar_orden" + " FROM dual";

		String resultadoFuncion = jdbcTemplateOracle.queryForObject(sqlFuncionServiceActivarOrden, String.class,
				vtaAutoActDTO.getIdAutoAct());
		if (resultadoFuncion.substring(0, 5).equals("ERROR")) {
			//rollback();
			String error = String.format("%s. No se pudo Activar la Orden", resultadoFuncion);
			GeneradorPlano.escribirLinea(error);
			
			if (origen.equals("EORDER")) {
				//codOperacion = parametros.getCodErrSY099();
				//observacion = parametros.getDesErrSY099();
				//vtmpError=error;
				//guardarTransferenciaOrden(registroOrden, parametros, codOperacion, observacion); /* INC000111911107 JAAG 03082023 Se añade el rechazo*/
			}
			
		} else {
			log.info("Resultado de funcion vta_activar_orden : " + resultadoFuncion);
			//commit();
			// R.I. REQSCOM03 Modificar Proceso Auto Activación 15/09/2023 INICIO
			// Originalmente siempre se escribía Activacion OK
			// Ahora escribe Entrega OK en el flujo ODT
			if (origen.equals("EORDER")) {
				// Flujo normal
				GeneradorPlano.escribirLinea("Entrega OK");
				//codOperacion = parametros.getCodErrSY000();
				//observacion = parametros.getDesErrSY000();
				//vtmpError="";
				//guardarTransferenciaOrden(registroOrden, parametros, codOperacion, observacion);
			} else {// INC000111911107 JAAG 03082023
				// Flujo ODT
				GeneradorPlano.escribirLinea("Entrega OK");
			}
			// R.I. REQSCOM03 Modificar Proceso Auto Activación 15/09/2023 FIN
			
			// homologacion con tablas scom
			if (resultadoFuncion.substring(0, 2).equals("OK")) {
				log.info("Resultado OK Ingresa en lógica de homologación");
				//contOrdActivadas++;
				//listaOrdVtaActiv.add(numOrden);
				//listaOrdCnxActiv.add(numOrdConex);
				/*
				if (numOrden != null) {
					Boolean homologacion = homologacionDe4JaScom(numOrden);
					log.info("la homologacion fue " + homologacion);
					// R.I. REQSCOM10 24/08/2023 INICIO
					commit();
					// R.I. REQSCOM10 24/08/2023 FIN;
				} else {
					log.info("no se pudo realizar la homologacion ya que numOrden es " + numOrden);
				}
				*/
			}
		}
		//cant++;

		String resultado = resultadoFuncion.substring(0, 5);
		log.info("variable resultado -> " + resultado);
		
		GeneradorPlano.subirAServidorFTP();
		System.exit(SUCCESS_CODE);
	}

	private void testFTP(String[] args) {
		ParametrosEntrada parametrosEntrada = ProcesadorEntrada.procesar(Arrays.asList(args));
		ParametrosDTO parametrosDTO = parametrosDAO.obtenerParametros(EMPRESA, USUARIO);
		GeneradorPlano.generar(parametrosEntrada, parametrosDTO.getFechaActual(), parametrosDTO.getPathLog());
		GeneradorPlano.escribirLinea("Probando");
		GeneradorPlano.subirAServidorFTP();
		System.exit(SUCCESS_CODE);
	}

	private void test() {
		
		//List<RegistroOrdenDTO> regs = LectorPlano.obtenerOrdenes("/tmp/bryan/prueba5.txt");
		List<RegistroOrdenDTO> regs = LectorPlano.obtenerOrdenes("d:\\prueba3.txt", java.util.Optional.empty());
		String ptoEntrega = regs.get(0).getPtoEntrega();
		log.info(ptoEntrega);
		log.info("{}", ptoEntrega.charAt(5));
		if (ptoEntrega.charAt(5) == '�') {
			ptoEntrega = "Emp.Aéreo. en BT ubicado frente al Predio";
		}
		log.info(Arrays.toString(regs.get(0).getPtoEntrega().getBytes()));
		log.info(Arrays.toString("Emp.Aéreo. en BT ubicado frente al Predio".getBytes()));
		log.info("{}", ptoEntrega.charAt(5));
		
		String sqlSelectIdPtoEntrega = SrvPtoEntregaMapper.SQLORACLE_SELECT_FOR_IDPTOENTREGA;
		SrvPtoEntregaDTO oSrvPtoEntrega = new SrvPtoEntregaDTO();
		SrvPtoEntregaMapper srvPtoEntregaMapper= new SrvPtoEntregaMapper();
		oSrvPtoEntrega=jdbcTemplateOracle.queryForObject(sqlSelectIdPtoEntrega,srvPtoEntregaMapper,ptoEntrega);
		log.info(oSrvPtoEntrega.getIdPtoEntrega().toString());
		System.exit(SUCCESS_CODE);
	}
}

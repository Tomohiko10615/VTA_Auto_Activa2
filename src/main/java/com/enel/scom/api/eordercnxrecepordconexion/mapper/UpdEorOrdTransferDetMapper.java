package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class UpdEorOrdTransferDetMapper {

	public static final String SQLPOSTGRE_UPDATE_FOR_ERRORORORDTRANSFERDET = "UPDATE"
			+ " schscom.eor_ord_transfer_det"
			+ " SET cod_error = ?"
			+ "	WHERE ID_ORD_TRANSFER = ?"
			+ "	AND accion = 'RECEPCION'"
			+ "	AND NRO_EVENTO = ?";

}

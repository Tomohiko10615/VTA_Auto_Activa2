package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EorOrdTransferDTO {

	private Long idOrdTransfer;
	private Long nroRecepciones;
	private String codTipoTDC;
}

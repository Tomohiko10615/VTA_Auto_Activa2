package com.enel.scom.api.eordercnxrecepordconexion.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MedidorNumMarModDTO {

	private String accionmedidor;
	// R.I. REQSCOM10 24/07/2023 INICIO
	private String factormedidor;
	// R.I. REQSCOM10 24/07/2023 FIN
	private String fechaconexion;
	private String nummedidor;
	private String modmedidor;
	private String marmedidor;
	
}

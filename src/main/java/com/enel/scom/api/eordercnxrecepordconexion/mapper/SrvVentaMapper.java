package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.SrvVentaDTO;

public class SrvVentaMapper implements RowMapper<SrvVentaDTO>{

	public static final String SQLORACLE_SELECT_FOR_SRVVENTA = "SELECT"
			+ " id_conf_pres_vta as idConfPres, id_ord_venta as idOrden, 0 count"
			+ " FROM vta_srv_venta"
			+ " WHERE id_srv_venta = ?";

	public static final String SQLORACLE_SELECT_FOR_SRVVENTA_COUNT ="SELECT 0 idConfPres, 0 idOrden,count(*) count\r\n"
			+ "            FROM vta_srv_venta\r\n"
			+ "           WHERE id_ord_venta = ?\r\n"
			+ "             AND estado in ('Generado', 'Inspeccionado')"; 
			
	@Override
	public SrvVentaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idConfPres = rs.getLong("idConfPres");
		Long idOrden = rs.getLong("idOrden");
		int count = rs.getInt("count");
		return new SrvVentaDTO(idConfPres, idOrden,count);
	}
	
}

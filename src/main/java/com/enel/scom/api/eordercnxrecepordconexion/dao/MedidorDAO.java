// R.I. REQSCOM10 21/07/2023 INICIO
package com.enel.scom.api.eordercnxrecepordconexion.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.enel.scom.api.eordercnxrecepordconexion.dto.MedidorNumMarModDTO;
import com.enel.scom.api.eordercnxrecepordconexion.mapper.MedidorMapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MedidorDAO {
	
	@Autowired
	@Qualifier("jdbcTemplate2")
	private JdbcTemplate jdbcTemplatePostgres;

	public boolean actualizarFactor(MedidorNumMarModDTO medidorNumMarModDTO) {
		String sqlScript = MedidorMapper.SQLPOSTGRESQL_UPDATE_FACTOR;
		int actualizados = 0;
		try {
			actualizados = jdbcTemplatePostgres.update(sqlScript, 
					medidorNumMarModDTO.getNummedidor(),
					medidorNumMarModDTO.getModmedidor(),
					medidorNumMarModDTO.getMarmedidor(),
					medidorNumMarModDTO.getModmedidor(),
					medidorNumMarModDTO.getMarmedidor(),
					medidorNumMarModDTO.getFactormedidor(),
					medidorNumMarModDTO.getNummedidor(),
					medidorNumMarModDTO.getModmedidor(),
					medidorNumMarModDTO.getMarmedidor(),
					medidorNumMarModDTO.getModmedidor(),
					medidorNumMarModDTO.getMarmedidor(),
					medidorNumMarModDTO.getFactormedidor());
		} catch (DataAccessException e) {
			log.error("Error al actualizar el factor: {}", e.getMessage());
		}
		return actualizados != 0;
	}

	public int obtenerNumFactoresValidos(MedidorNumMarModDTO medidorNumMarModDTO) {
		String sqlScript = MedidorMapper.SQLPOSTGRESQL_SELECT_COUNT_FACTOR;
		int numFactoresValidos = 0;
		try {
			numFactoresValidos = jdbcTemplatePostgres.queryForObject(sqlScript, Integer.class,
					medidorNumMarModDTO.getNummedidor(),
					medidorNumMarModDTO.getModmedidor(),
					medidorNumMarModDTO.getMarmedidor(),
					medidorNumMarModDTO.getModmedidor(),
					medidorNumMarModDTO.getMarmedidor(),
					medidorNumMarModDTO.getFactormedidor(),
					medidorNumMarModDTO.getNummedidor(),
					medidorNumMarModDTO.getModmedidor(),
					medidorNumMarModDTO.getMarmedidor(),
					medidorNumMarModDTO.getModmedidor(),
					medidorNumMarModDTO.getMarmedidor(),
					medidorNumMarModDTO.getFactormedidor());
		} catch (DataAccessException e) {
			log.error("Error al validar los factores: {}", e.getMessage());
		}
		return numFactoresValidos;
	}
}
// R.I. REQSCOM10 21/07/2023 FIN
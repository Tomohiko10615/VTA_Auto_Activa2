package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SrvLlaveMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_IDLLAVE = "SELECT"
			+ " ID_LLAVE"
			+ " FROM srv_llave"
			+ " WHERE descripcion= ?"
			+ " AND id_sed = ?"
			+ " AND activo = 'S'";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idLlave = rs.getLong("ID_LLAVE");
		return idLlave;
	}
	
}

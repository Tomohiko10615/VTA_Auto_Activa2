package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosProcesosDTO;

public class DatosProcesosMapper implements RowMapper<DatosProcesosDTO> {

	/*
	public static final String SQLPOSTGRESQL_SELECT_FOR_DATOSPROCESOS = "SELECT"
			+ " XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Distribuidora/text()', d.REG_XML)[1] AS codigoDistribuidora,"
			+ "	XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Sistema_Externo_de_Origen/text()', d.REG_XML)[1] AS codigoSistemaExternoOrigen,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Tipo_de_TdC/text()', d.REG_XML)[1] AS codigoTipoTdC,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Externo_del_TdC/text()', d.REG_XML)[1] AS codigoExternoTdC,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Interno_del_TdC/text()', d.REG_XML)[1] AS codigoInternoTdC,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Resultado/text()', d.REG_XML)[1] AS codigoResultado,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Causal_Resultado/text()', d.REG_XML)[1] AS codigoCausalResultado,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Nota_Codificada/text()', d.REG_XML)[1] AS codigoNotaCodificada,"
			+ "	XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Inicio_TdC/text()', d.REG_XML)[1] AS inicioTdC,"
			+ "	XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Fin_TdC/text()', d.REG_XML)[1] AS finTdC,"
			+ "	XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Duracion_Ejecucion/text()', d.REG_XML)[1] AS duracionEjecucion,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Cuadrilla/text()', d.REG_XML)[1] AS codigoCuadrilla,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/latitud_medidor/text()', d.REG_XML)[1] AS latitudMedidor,"
			+ " XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/longitud_medidor/text()', d.REG_XML)[1] AS longitudMedidor,"
			+ "	XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/latitud_empalme/text()', d.REG_XML)[1] AS latitudEmpalme,"
			+ "	XPATH('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/longitud_empalme/text()', d.REG_XML)[1] AS longitudEmpalme"
			+ " FROM schscom.eor_ord_transfer_det d"
			+ " WHERE d.id_ord_transfer = ?"
			+ "	and accion = 'RECEPCION'"
			+ "	and nro_evento = ?"; 
*/
	public static final String SQLPOSTGRESQL_SELECT_FOR_DATOSPROCESOS = "SELECT"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Distribuidora/text()', d.REG_XML))::text AS codigoDistribuidora,"
			+ "                 unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Sistema_Externo_de_Origen/text()', d.REG_XML))::text AS codigoSistemaExternoOrigen,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Tipo_de_TdC/text()', d.REG_XML))::text AS codigoTipoTdC,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Externo_del_TdC/text()', d.REG_XML))::text AS codigoExternoTdC,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosClaveProcesosTDC/Codigo_Interno_del_TdC/text()', d.REG_XML))::text AS codigoInternoTdC,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Resultado/text()', d.REG_XML))::text AS codigoResultado,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Causal_Resultado/text()', d.REG_XML))::text AS codigoCausalResultado,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Nota_Codificada/text()', d.REG_XML))::text AS codigoNotaCodificada,"
			+ "                 unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Inicio_TdC/text()', d.REG_XML))::text AS inicioTdC,"
			+ "                 unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Fin_TdC/text()', d.REG_XML))::text AS finTdC,"
			+ "                 unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Duracion_Ejecucion/text()', d.REG_XML))::text AS duracionEjecucion,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Cuadrilla/text()', d.REG_XML))::text AS codigoCuadrilla,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/latitud_medidor/text()', d.REG_XML))::text AS latitudMedidor,"
			+ "              unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/longitud_medidor/text()', d.REG_XML))::text AS longitudMedidor,"
			+ "                 unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/latitud_empalme/text()', d.REG_XML))::text AS latitudEmpalme,"
			+ "                 unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/longitud_empalme/text()', d.REG_XML))::text AS longitudEmpalme"
			+ "              FROM schscom.eor_ord_transfer_det d"
			+ "              WHERE d.id_ord_transfer = ?"
			+ "                 and accion = 'RECEPCION'"
			+ "                 and nro_evento = ?"; 
	
	@Override
	public DatosProcesosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String codigoDistribuidora = rs.getString("codigoDistribuidora");
		String codigoSistemaExternoOrigen = rs.getString("codigoSistemaExternoOrigen");
		String codigoTipoTdC = rs.getString("codigoTipoTdC");
		String codigoExternoTdC = rs.getString("codigoExternoTdC");
		String codigoInternoTdC = rs.getString("codigoInternoTdC");
		String codigoResultado = rs.getString("codigoResultado");
		String codigoCausalResultado = rs.getString("codigoCausalResultado");
		String codigoNotaCodificada = rs.getString("codigoNotaCodificada");
		String inicioTdC = rs.getString("inicioTdC");
		String finTdC = rs.getString("finTdC");
		String duracionEjecucion = rs.getString("duracionEjecucion");
		String codigoCuadrilla = rs.getString("codigoCuadrilla");
		String latitudMedidor = rs.getString("latitudMedidor");
		String longitudMedidor = rs.getString("longitudMedidor");
		String latitudEmpalme = rs.getString("latitudEmpalme");
		String longitudEmpalme = rs.getString("longitudEmpalme");
		return new DatosProcesosDTO(codigoDistribuidora, codigoSistemaExternoOrigen, codigoTipoTdC, codigoExternoTdC, codigoInternoTdC, codigoResultado, codigoCausalResultado,
				codigoNotaCodificada, inicioTdC, finTdC, duracionEjecucion, codigoCuadrilla, latitudMedidor, longitudMedidor, latitudEmpalme, longitudEmpalme);
	}
	
}

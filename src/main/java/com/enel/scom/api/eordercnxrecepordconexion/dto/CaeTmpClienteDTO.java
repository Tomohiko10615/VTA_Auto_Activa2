package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class CaeTmpClienteDTO {

	private Long ID_CAE_TMP_CLIENTE;
	private Long NUMERO_CLIENTE;
	private String NOMBRE;
	private String TARIFA;
	private Long SUCURSAL;
	private String TELEFONO;
	private String DIRECCION;

	private String SECTOR;
	private String ZONA;
	private String CORR_RUTA;
	
	private String COD_GIRO;
	private String TIPO_CLIENTE;
	private String COMUNA;
	private String RUT;
	private String TIPO_IDENT;
	private String ESTADO_CLIENTE;
	private String ESTADO_SUMINISTRO;

	private String FECHA_ACTIVACION;
	private String FECHA_RETIRO;

	private Long DV_NUMERO_CLIENTE;
	private String INFO_ADIC_LECTURA;
	private Long POTENCIA_CONT_FP;
	private Long POTENCIA_INST_FP;
	private Long POTENCIA_INST_HP;

	private Long ID_PCR;
	private Long PCR;
	private String LLAVE;

	private String SUBESTAC_DISTRIB;
	private String ALIMENTADOR;
	private String SUBESTAC_TRANSMI;
	private String TIPO_ACCION;
	private String ESTADO_PROCESO;
	private String MOTIVO_RECHAZO;
	private Long NRO_ENVIOS;

}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.RutaAutLecturaDTO;

public class RutaAutLecturaMapper implements RowMapper<RutaAutLecturaDTO> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_RUTAUTLECTURA = "SELECT"
			+ " TIPO as tipoSol, NRO_SOLICITUD as solicitud, 0 codRutaAuto"
			+ "	FROM schscom.RUTA_AUT_LECTURA"
			+ "	WHERE NRO_SOLICITUD = ?"
			+ "	group by TIPO, NRO_SOLICITUD";
	
	public static final String SQLPOSTGRESQL_SELECT_FOR_CODRUTAAUTO = "SELECT '' tipoSol, 0 solicitud,"
			+ " NVL(ID_RUTA,0) as codRutaAuto"
			+ "	FROM schscom.RUTA_AUT_LECTURA"
			+ "	WHERE"
			+ "	NRO_SOLICITUD = ?"
			+ "	AND TIPO = 'E'";
	
	public static final String SQLPOSTGRESQL_SELECT_FOR_CODRUTAAUTO2 = "SELECT '' tipoSol, 0 solicitud,"
			+ " NVL(ID_RUTA,0) as codRutaAuto"
			+ "	FROM schscom.RUTA_AUT_LECTURA"
			+ "	WHERE"
			+ "	NRO_SOLICITUD = ?"
			+ "	AND NRO_SUMINISTRO = ?"
			+ "	AND TIPO = 'C'";

	@Override
	public RutaAutLecturaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String tipoSol = rs.getString("tipoSol");
		Long solicitud = rs.getLong("solicitud");
		Long codRutaAuto = rs.getLong("codRutaAuto");
		return new RutaAutLecturaDTO(tipoSol, solicitud,codRutaAuto);
	}
	
}

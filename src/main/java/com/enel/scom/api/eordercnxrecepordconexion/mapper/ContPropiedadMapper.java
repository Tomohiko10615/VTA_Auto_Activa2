package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ContPropiedadMapper implements RowMapper<Integer> {

	public static final String SQLORACLE_SELECT_FOR_CONTPROPIEDAD = "SELECT"
			+ " COUNT(1) as contPropiedad"
			+ "	FROM PRD_PROPIEDAD"
			+ "	WHERE ID_PRODUCTO = ?"
			+ "	AND TIP_PROPIEDAD = 'CAM'"
			+ "	AND VAL_PROPIEDAD = 'S'";

	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Integer contPropiedad = rs.getInt("contPropiedad");
		return contPropiedad;
	}
	
}

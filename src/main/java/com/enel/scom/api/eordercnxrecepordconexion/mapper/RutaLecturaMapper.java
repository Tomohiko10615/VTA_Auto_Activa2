package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenVentaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.RutaLecturaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.UbiRutaDTO;

public class RutaLecturaMapper implements RowMapper<RutaLecturaDTO> {

	public static final String SQLORACLE_SELECT_FOR_RUTALECTURA = "SELECT to_number(substr(?,2,2)) as vsector , to_number(substr(?,4,3)) as vzona, to_number(substr(?,7,4)) as vcorrelativo \r\n"
			+ "			FROM dual";

	@Override
	public RutaLecturaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long vsector = rs.getLong("vsector");
		Long vzona = rs.getLong("vzona");
		Long vcorrelativo = rs.getLong("vcorrelativo");
		return new RutaLecturaDTO(vsector, vzona, vcorrelativo);
	}
	
	
}

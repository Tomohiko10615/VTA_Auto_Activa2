package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.ValidarSolicitudDTO;

public class ValidarSolicitudMapper implements RowMapper<ValidarSolicitudDTO> {

	public static final String SQLORACLE_SELECT_FOR_VALIDARSOLICITUD = "SELECT"
			+ " a.id_orden as idRegistro, b.id_state as codigo, TO_CHAR(d.fecha_ingreso,'dd/mm/yyyy') as fechaIngreso"
			+ " FROM vta_solicitud d, ord_orden a, wkf_workflow b, vta_ord_vta c"
			+ " WHERE d.id_solicitud = c.id_solicitud"
			+ " AND a.id_tipo_orden=5"
			+ "	AND a.id_empresa=3"
			+ " AND a.nro_orden = ?"
			+ " AND a.id_workflow=b.id_workflow"
			+ " AND a.id_orden=c.id_ord_venta"
			+ " AND c.paralizada='N'";

	public static final String SQLORACLE_SELECT_FOR_VALIDARSOLICITUD2 = "SELECT"
			+ " a.id_orden as idRegistro, b.id_state as codigo, TO_CHAR(d.fecha_ingreso,'dd/mm/yyyy') as fechaIngreso"
			+ "	FROM vta_solicitud d, ord_orden a, wkf_workflow b, vta_ord_vta c"
			+ " WHERE d.id_solicitud = c.id_solicitud"
			+ " AND a.id_tipo_orden=5"
			+ "	AND a.id_empresa=3"
			+ "	AND a.nro_orden = ?"
			+ "	AND a.id_workflow=b.id_workflow"
			+ "	AND a.id_orden=c.id_ord_venta"
			+ "	AND c.paralizada='S'"
			+ "	AND b.ID_STATE IN ('Ejecucion', 'PorEjecutar','RecEjecucion')";
	
	
	@Override
	public ValidarSolicitudDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		Long idRegistro = rs.getLong("idRegistro");
		String codigo = rs.getString("codigo");
		String fechaIngreso = rs.getString("fechaIngreso");
		return new ValidarSolicitudDTO(idRegistro, codigo, fechaIngreso);
	}

}

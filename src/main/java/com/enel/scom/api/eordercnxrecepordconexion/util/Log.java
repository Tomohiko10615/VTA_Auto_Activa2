package com.enel.scom.api.eordercnxrecepordconexion.util;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Log {
	public static FileWriter file1;
	public static FileWriter file1Error;
	public static FileWriter file2Error;
	 public static PrintWriter file1Writer;
	 public static PrintWriter file1WriterError;
	 public static PrintWriter file2WriterError;
	    
	public static boolean bfnOpenLogFile(String proceso,String szPath)
	{
		System.out.println("\nproceso: "+ proceso+"\nszPath: " +szPath+"\n");
		
		String mszFileName;
		//String szPath;
		String szArchivo;
		String mszTime;
		String ptTime;
		
		
		int TIME_LEN=19;
		String TIME_FILENAME=obtenerFechaYHoraActual();
		System.out.println("TIME_FILENAME: "+TIME_FILENAME);
		
		//szPath="D:/PRUEBA/";
		szArchivo=proceso;
		//ptTime=LocalTime.now();
		mszTime=(TIME_LEN+1)+TIME_FILENAME;
		
		mszFileName=szPath+
				szArchivo+"_"+
				mszTime+"_"+
				".log";
				//getpid()+
		System.out.println("mszFileName: "+mszFileName);
		
		return mbfnOpenFile(mszFileName);
	}
	
	private static boolean mbfnOpenFile(String gcNomArchivoOUT)
	{
		 try {
	            file1 = new FileWriter(gcNomArchivoOUT);
	            file1Writer = new PrintWriter(file1);
	        } catch (Exception e) {
	        	logError(String.format("No se pudo crear archivo de salida[%s]: %s\n", gcNomArchivoOUT, e.getMessage()));
	            
	            System.exit(1);
	        }
		
		return true;
	}
	
	public static boolean bfnCloseLogFile()
	{
		try
		{
			file1Writer.close();
		}catch (Exception ex)
		{
			logError("Error al cerrar el log\\n");
			return false;
		}
		return true;
	}
		
	public static String obtenerFechaYHoraActual() {
		String formato = "yyyyMMdd_HHmmss";
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern(formato);
		LocalDateTime ahora = LocalDateTime.now();
		return formateador.format(ahora);
	}
	
	public static boolean ifnMsgToLogFile(String mensaje)
	{
		try
		{
			System.out.println(mensaje);
		file1Writer.println(mensaje);
		}
		catch(Exception ex)
		{
			return false;
		}
		return true;
	}
	
	public static boolean bfnOpenLogFileError(String proceso,String szPath)
	{
		System.out.println("\nproceso: "+ proceso+"\nszPath: " +szPath+"\n");
		
		String mszFileName;
		//String szPath;
		String szArchivo;
		String mszTime;
		String ptTime;
		
		
		int TIME_LEN=19;
		String TIME_FILENAME=obtenerFechaYHoraActual();
		System.out.println("TIME_FILENAME: "+TIME_FILENAME);
		
		//szPath="D:/PRUEBA/";
		szArchivo=proceso;
		//ptTime=LocalTime.now();
		mszTime=(TIME_LEN+1)+TIME_FILENAME;
		
		mszFileName=szPath+
				szArchivo+"_"+
				mszTime+"_"+
				"ERROR"+
				".log";
				//getpid()+
		System.out.println("mszFileName: "+mszFileName);
		
		return mbfnOpenFileError(mszFileName);
	}
	
	public static boolean mbfnOpenFileError(String gcNomArchivoOUTError)
	{		 
		 try {
	            file1Error = new FileWriter(gcNomArchivoOUTError);
	            file1WriterError = new PrintWriter(file1Error);
	        } catch (Exception e) {
	        	logError(String.format("No se pudo crear archivo de salida[%s]: %s\n", gcNomArchivoOUTError, e.getMessage()));
	            
	            System.exit(1);
	        }
		return true;
	}
	public static boolean mbfnOpenFileError2(String gcNomArchivoOUTError)
	{		 
		 try {
	            file2Error = new FileWriter(gcNomArchivoOUTError);
	            file2WriterError = new PrintWriter(file2Error);
	        } catch (Exception e) {
	        	logError(String.format("No se pudo crear archivo de salida[%s]: %s\n", gcNomArchivoOUTError, e.getMessage()));
	            
	            System.exit(1);
	        }
		return true;
	}
	
	public static boolean logError(String lineaError)
	{
		try
		{
			System.out.println(lineaError);
			file1WriterError.println(lineaError);
		}
		catch(Exception ex)
		{
			System.out.println("ex: "+ex);
			return false;
		}
		return true;
	}
	public static boolean logError2(String lineaError)
	{
		try
		{
			System.out.println(lineaError);
			file2WriterError.println(lineaError);
		}
		catch(Exception ex)
		{
			System.out.println("ex: "+ex);
			return false;
		}
		return true;
	}
	
	public static boolean bfnCloseLogFileError()
	{
		try
		{
			file1WriterError.close();
		}catch (Exception ex)
		{
			logError("Error al cerrar el log Error\\n");
			return false;
		}
		return true;
	}
	public static boolean bfnCloseLogFileError2()
	{
		try
		{
			file2WriterError.close();
		}catch (Exception ex)
		{
			logError("Error al cerrar el log Error\\n");
			return false;
		}
		return true;
	}
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.CaeTmpClienteDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenVentaDTO;

public class CaeTmpClienteMapper implements RowMapper<CaeTmpClienteDTO> {

	public static final String SQLORACLE_SELECT_FOR_CAETMPCLIENTE4J = " select ID_CAE_TMP_CLIENTE, NUMERO_CLIENTE, NOMBRE, TARIFA, SUCURSAL, TELEFONO, DIRECCION, SECTOR, ZONA, CORR_RUTA,\r\n"
			+ " COD_GIRO, TIPO_CLIENTE, COMUNA, RUT, TIPO_IDENT, ESTADO_CLIENTE, ESTADO_SUMINISTRO, FECHA_ACTIVACION, FECHA_RETIRO,\r\n"
			+ " DV_NUMERO_CLIENTE, INFO_ADIC_LECTURA, POTENCIA_CONT_FP, POTENCIA_INST_FP, POTENCIA_INST_HP, ID_PCR, PCR, LLAVE,\r\n"
			+ " SUBESTAC_DISTRIB, ALIMENTADOR, SUBESTAC_TRANSMI, TIPO_ACCION, ESTADO_PROCESO, MOTIVO_RECHAZO, NRO_ENVIOS\r\n"
			+ " from CAE_TMP_CLIENTE\r\n"
			+ " where NUMERO_CLIENTE = ?";

	@Override
	public CaeTmpClienteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long ID_CAE_TMP_CLIENTE = rs.getLong("ID_CAE_TMP_CLIENTE");
		Long NUMERO_CLIENTE = rs.getLong("NUMERO_CLIENTE");
		String NOMBRE = rs.getString("NOMBRE");
		String TARIFA = rs.getString("TARIFA");
		Long SUCURSAL = rs.getLong("SUCURSAL");
		String TELEFONO = rs.getString("TELEFONO");
		String DIRECCION = rs.getString("DIRECCION");

		String SECTOR = rs.getString("SECTOR");
		String ZONA = rs.getString("ZONA");
		String CORR_RUTA = rs.getString("CORR_RUTA");
		
		String COD_GIRO = rs.getString("COD_GIRO");
		String TIPO_CLIENTE = rs.getString("TIPO_CLIENTE");
		String COMUNA = rs.getString("COMUNA");
		String RUT = rs.getString("RUT");
		String TIPO_IDENT = rs.getString("TIPO_IDENT");
		String ESTADO_CLIENTE = rs.getString("ESTADO_CLIENTE");
		String ESTADO_SUMINISTRO = rs.getString("ESTADO_SUMINISTRO");

		String FECHA_ACTIVACION = rs.getString("FECHA_ACTIVACION");
		String FECHA_RETIRO = rs.getString("FECHA_RETIRO");

		Long DV_NUMERO_CLIENTE = rs.getLong("DV_NUMERO_CLIENTE");
		String INFO_ADIC_LECTURA = rs.getString("INFO_ADIC_LECTURA");
		Long POTENCIA_CONT_FP = rs.getLong("POTENCIA_CONT_FP");
		Long POTENCIA_INST_FP = rs.getLong("POTENCIA_INST_FP");
		Long POTENCIA_INST_HP = rs.getLong("POTENCIA_INST_HP");

		Long ID_PCR = rs.getLong("ID_PCR");
		Long PCR = rs.getLong("PCR");
		String LLAVE = rs.getString("LLAVE");

		String SUBESTAC_DISTRIB = rs.getString("SUBESTAC_DISTRIB");
		String ALIMENTADOR = rs.getString("ALIMENTADOR");
		String SUBESTAC_TRANSMI = rs.getString("SUBESTAC_TRANSMI");
		String TIPO_ACCION = rs.getString("TIPO_ACCION");
		String ESTADO_PROCESO = rs.getString("ESTADO_PROCESO");
		String MOTIVO_RECHAZO = rs.getString("MOTIVO_RECHAZO");
		Long NRO_ENVIOS = rs.getLong("NRO_ENVIOS");
		return new CaeTmpClienteDTO(ID_CAE_TMP_CLIENTE, NUMERO_CLIENTE, NOMBRE, TARIFA, SUCURSAL, TELEFONO,
				DIRECCION, SECTOR, ZONA, CORR_RUTA, COD_GIRO, TIPO_CLIENTE, COMUNA, RUT, TIPO_IDENT, ESTADO_CLIENTE,
				ESTADO_SUMINISTRO, FECHA_ACTIVACION, FECHA_RETIRO, DV_NUMERO_CLIENTE, INFO_ADIC_LECTURA,
				POTENCIA_CONT_FP, POTENCIA_INST_FP, POTENCIA_INST_HP, ID_PCR, PCR, LLAVE, SUBESTAC_DISTRIB,
				ALIMENTADOR, SUBESTAC_TRANSMI, TIPO_ACCION, ESTADO_PROCESO, MOTIVO_RECHAZO, NRO_ENVIOS
				);
	}
	
	
}

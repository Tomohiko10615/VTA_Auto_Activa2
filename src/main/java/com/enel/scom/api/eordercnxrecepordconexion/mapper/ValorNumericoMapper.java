package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ValorNumericoMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_VALNUMERICO = "SELECT"
			+ " VALOR_NUM"
			+ "	FROM COM_PARAMETROS"
			+ "	WHERE SISTEMA= ?"
			+ "	AND ENTIDAD = ?"
			+ "	AND CODIGO = ?";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		Long valorNum = rs.getLong("VALOR_NUM");
		return valorNum;
	}
	
}

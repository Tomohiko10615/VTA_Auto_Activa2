package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrdServVentaMapper implements RowMapper<String> {

	public static final String SQLORACLE_SELECT_FOR_ORDENSRVVENTA = "SELECT"
			+ " ORD.NRO_ORDEN as orden"
			+ "	FROM ORD_ORDEN ORD, ORD_CONEX OCNX"
			+ "	WHERE ORD.ID_ORDEN = OCNX.ID_ORD_VENTA"
			+ "	AND OCNX.NRO_ORDEN = ?"
			+ "	and rownum = 1";

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		String orden = rs.getString("orden");
		return orden;
	}
	
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenVentaDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.UbiRutaDTO;

public class UbiRutaMapper implements RowMapper<UbiRutaDTO> {

	public static final String SQLORACLE_SELECT_FOR_UBIRUTA = "SELECT id_nodo, min(to_number(substr(cod_ruta,7,4))) as corr_ini,\r\n"
			+ "                              max(to_number(substr(cod_ruta,7,4))) as corr_fin, 0 count \r\n"
			+ "                FROM ubi_ruta\r\n"
			+ "               WHERE id_ruta in(?, ?)\r\n"
			+ "               GROUP BY id_nodo";
	
	public static final String SQLORACLE_SELECT_FOR_COUNT_UBIRUTA = "SELECT count(distinct id_nodo) count \r\n"
			+ " , '' corr_ini, '' corr_fin , 0 id_nodo"
			+ "				   FROM ubi_ruta\r\n"
			+ "				  WHERE id_ruta in(?,?)";

	@Override
	public UbiRutaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idNodo = rs.getLong("id_nodo");
		int corrIni = rs.getInt("corr_ini");
		int corrFin = rs.getInt("corr_fin");
		int count = rs.getInt("count");
		return new UbiRutaDTO(idNodo, corrIni, corrFin,count);
	}
	
	
}

package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DatosComunesDTO {

	private String codigoOperacion="";
	private String codigoResultado="";
	private String codigoCausalResultado="";
	private String codigoNotaCodificada="";
	private String inicioOperacion="";
	private String finOperacion="";
	private String duracionOperacion="";
	private String notasOperacion="";
	
}

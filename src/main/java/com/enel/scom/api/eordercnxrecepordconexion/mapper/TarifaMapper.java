package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TarifaMapper implements RowMapper<String>{

	// R.I. 18/08/2023 Se elimina srv_electrico de la consulta
	// R.I. CORRECCION 12/09/2023 Nueva consulta para obtener la tarifa
	public static final String SQLORACLE_SELECT_FOR_TARIFA = " SELECT DISTINCT SUBSTR(c.COD_TARIFA,1,3) as codTarifa\r\n"
			+ "    FROM VTA_SRV_VENTA SVEN, ORD_CONEX OCNX, VTA_SOL_SRV_ELE B, fac_tarifa c\r\n"
			+ "    WHERE\r\n"
			+ "        SVEN.ID_ORD_CONEX = OCNX.ID_ORD_CONEX\r\n"
			+ "        AND SVEN.ID_SOL_SRV_ELE = B.ID_SOL_SRV_ELE\r\n"
			+ "        AND OCNX.NRO_ORDEN = (SELECT\r\n"
			+ " OCNX.NRO_ORDEN as nroOrdConex\r\n"
			+ " FROM ORD_ORDEN OVEN, VTA_SRV_VENTA SVEN, ORD_CONEX OCNX, VTA_SOL_SRV_ELE SELE, NUC_CUENTA nc\r\n"
			+ " WHERE\r\n"
			+ " OVEN.ID_ORDEN = SVEN.ID_ORD_VENTA\r\n"
			+ " AND SVEN.ID_ORD_CONEX = OCNX.ID_ORD_CONEX\r\n"
			+ " AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE\r\n"
			+ " AND SELE.ID_CUENTA_FAC = NC.ID_CUENTA\r\n"
			+ " AND OVEN.NRO_ORDEN = ?\r\n"
			+ " AND NC.NRO_CUENTA = ?)\r\n"
			+ "        AND b.ID_TARIFA =c.ID_TARIFA";

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String codTarifa = rs.getString("codTarifa");
		return codTarifa;
	}
	
}

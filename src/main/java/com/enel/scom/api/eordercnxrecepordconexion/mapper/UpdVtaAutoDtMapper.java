package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.VtaAutoDtDTO;

public class UpdVtaAutoDtMapper implements RowMapper<VtaAutoDtDTO>{

	public static final String SQLORACLE_UPDATE_FOR_VTAAUTODT = "UPDATE"
			+ " vta_auto_dt"
			+ "	SET caja_toma = ?"
			+ "	WHERE id_auto_dt = ?";
	
	public static final String SQLORACLE_UPDATE_FOR_VTAAUTODT2 = "UPDATE"
			+ " vta_auto_dt"
			+ "	SET id_srv_ele_der = ?"
			+ "	WHERE id_auto_dt = ?";
	 
	public static final String SQLORACLE_UPDATE_FOR_VTAAUTODT3 = "UPDATE"
			+ " vta_auto_dt"
			+ "	SET id_srv_ele_izq = ?"
			+ "	WHERE id_auto_dt = ?";
	
	public static final String SQLORACLE_SELECT_COUNT_VTAAUTODT ="SELECT count(*)\r\n"
			+ "            as cant\r\n"
			+ "            FROM vta_auto_dt\r\n"
			+ "           WHERE id_srv_venta = ? ";
	
	@Override
	public VtaAutoDtDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		VtaAutoDtDTO oVtaAutoDtDTO= new VtaAutoDtDTO();
		oVtaAutoDtDTO.setCont(rs.getInt("cant"));
		return oVtaAutoDtDTO;
	}
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class InsSrvElectricoMapper {

	public static final String SQLORACLE_INSERT_FOR_SRVELECTRICO = "INSERT INTO EOR_SRV_ELEC_XY"
			+ " (ID_SERVICIO, LATITUD, LONGITUD, LATITUD_PCR, LONGITUD_PCR, FECHA_ACTUALIZA)"
			+ "	VALUES"
			+ "	(?, ?, ?, ?, ?, SYSDATE)";
}

package com.enel.scom.api.eordercnxrecepordconexion.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ArchivoUtil {
	
	public static FileWriter file1;
	public static PrintWriter file1Writer;	
	
	public static FileWriter file2;
	public static PrintWriter file2Writer;

	public static FileWriter file3;
	public static PrintWriter file3Writer;
	
    public static String gcEjecutor;
    public static String gcContratista;
    public static String gcPathOUT;
    public static String gcNameArchivo;
    public static String gcPathAnexo;
    public static String gcNomArchivoERR;
    public static String gcPathEjecutables;
    public static String gcFecha;
    public static String gcFechaInicio;
    public static String gcFechaFin;
    public static String gcExtension;
    
    public static String gcNomArchivoOUT;
    
    private static int lFilasCorrectas;
    private static int lFilasProc;
    private static int lFilasErr;
    private static int iIndicaErr;
    
    private static String vTieneAnexos;
    private static String vCodEstadoAnexo;
    private static String vCodEstPendRecepAnexos;
	
	public static void crearArchivo(String Path_Procesado, Long NroODT) {
		
		String rutaAuxPruebas = "";
		String archivoR;
		String archivolog;

		// Archivo de entrada para proceso de Autoactivacion
		/*
		String nombrearchivo = String.format("%s%sEOR_CNX_ORD_CONEXION_ACTIVAR_%d.xls", rutaAuxPruebas, Path_Procesado, NroODT);
		log.info("Creando Archivo de entrada para proceso de Autoactivacion " + nombrearchivo);
		
        try {
            file1 = new FileWriter(nombrearchivo);
            file1Writer = new PrintWriter(file1);
        } catch (IOException e) {
            log.error(String.format("No se pudo crear archivo de salida[%s]: %s\n", nombrearchivo, e.getMessage()));
            System.exit(1);
        }
        
        */
        // Archivo de Ordenes pendientes de activacion
		String nombrearchivoR = String.format("%s%sEOR_CNX_ORD_CONEXION_PENDIENTES_ACTIVAR%d.xls", rutaAuxPruebas, Path_Procesado, NroODT);
        log.info("Creando Archivo de Ordenes pendientes de activacion" + nombrearchivoR);
        
        try {
            file2 = new FileWriter(nombrearchivoR);
            file2Writer = new PrintWriter(file2);
        } catch (IOException e) {
            log.error(String.format("No se pudo crear archivo de salida[%s]: %s\n", nombrearchivoR, e.getMessage()));
            System.exit(1);
        }
        
        // Archivo Log
        /*
        String nombrearchivolog = String.format("%s%sEOR_CNX_RECEP_ORD_CONEXION_ERRORES_%d.xls", rutaAuxPruebas, Path_Procesado, NroODT);
		log.info("Creando Escritura Archivo Log" + nombrearchivolog);
		
        try {
            file3 = new FileWriter(nombrearchivolog);
            file3Writer = new PrintWriter(file3);
        } catch (IOException e) {
            log.error(String.format("No se pudo crear archivo de salida[%s]: %s\n", nombrearchivolog, e.getMessage()));
            System.exit(1);
        }
         */
	}
	
	
	public static boolean bfnAbrirArchivo()
	{
		String gcNomArchivoOUT;        
        
        gcNomArchivoOUT = String.format("%s%s%s","D:/"+gcPathOUT,gcNameArchivo,gcExtension);//gcPathOUT
        log.info(String.format("RUTA DE ARCHIVO: %s\n", gcNomArchivoOUT));

        try {
            file1 = new FileWriter(gcNomArchivoOUT);
            file1Writer = new PrintWriter(file1);
        } catch (IOException e) {
            log.error(String.format("No se pudo crear archivo de salida[%s]: %s\n", gcNomArchivoOUT, e.getMessage()));
            System.exit(1);
        }
        return true;
	}
	
	public static void imprimirTitulo()
	{		
		file1Writer.printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t\n",
				"Código Externo del TdC", "Código Interno del TdC", "Código Resultado", "Código Causal Resultado", "Codigo Nota Codificada",
                "Inicio TdC", "Fin TdC", "Duración Ejecución", "Código Cuadrilla",
                "Observaciones", "Nombre Anexo", "Nro Cuenta", "Número Medidor",
                "Marca Medidor", "Modelo Medidor", "Tipo Medidor",
                "Acción Medidor", "Año Fabricación", "Ubicación Medidor", "Latitud",
                "Longitud","Se ubicó al cliente","Lectura","Condición de vivienda","Descripción del predio",
                "Número de Pisos del predio","Color de vivienda","Aledaño izquierdo","Aledaño derecho","Tiene suministro adicional",
                "Telefono cliente","Tiene Luz","Tiene Mica","Tiene tapa","Recepcionado por",
                "Número del documento del cliente (por ejemplo DNI)","Tipo_Dcto_Ident","Fecha entrega a Contratista");		
	}
	
	/*
	public static void imprimirArchivoPlano(ArchivoDetalle oArchivoDetalle) {
		log.info("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t\n",
				oArchivoDetalle.getCodExtTdc(), 
				oArchivoDetalle.getCodIntTdc(),
				oArchivoDetalle.getCodResultado(), 
				oArchivoDetalle.getCodCausalResultado(), 
				oArchivoDetalle.getCodNotaCodificada(),
				oArchivoDetalle.getInicioTdc(),
				oArchivoDetalle.getFinTdc(),
				oArchivoDetalle.getDuracionEjecucion(),
				oArchivoDetalle.getCodCuadrilla(),
				oArchivoDetalle.getObservaciones(), 
				oArchivoDetalle.getNombreAnexo(),
				oArchivoDetalle.getNroCuenta(),
				oArchivoDetalle.getNroMedidor(),
				oArchivoDetalle.getMarcaMedidor(), 
				oArchivoDetalle.getModeloMedidor(),
				oArchivoDetalle.getTipoMedidor(),
				oArchivoDetalle.getAccionMedidor(),
				oArchivoDetalle.getAnioFabricacion(),
				oArchivoDetalle.getUbicacionMedidor(),
				oArchivoDetalle.getLatitud(),
				oArchivoDetalle.getLongitud(),
				oArchivoDetalle.getSeUbicoCliente(),
				oArchivoDetalle.getLectura(),
				oArchivoDetalle.getCondicionVivienda(),
				oArchivoDetalle.getDescripcionPredio(),
				oArchivoDetalle.getNroPisosPredio(),
				oArchivoDetalle.getColorVivienda(),
				oArchivoDetalle.getAledanioIzquierdo(),
				oArchivoDetalle.getAledanioDerecho(),
				oArchivoDetalle.getTieneSuministroAdicional(),
				oArchivoDetalle.getTelefonoCliente(),
				oArchivoDetalle.getTieneLuz(),
				oArchivoDetalle.getTieneMica(),
				oArchivoDetalle.getTieneTapa(),
				oArchivoDetalle.getRecepcionadoPor(),
				oArchivoDetalle.getNroDocumentoCliente(),
				oArchivoDetalle.getTipoDctoIdent(),
				oArchivoDetalle.getFechaEntregaContratista()
                );
		
		file1Writer.printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t\n",
				oArchivoDetalle.getCodExtTdc(), 
				oArchivoDetalle.getCodIntTdc(),
				oArchivoDetalle.getCodResultado(), 
				oArchivoDetalle.getCodCausalResultado(), 
				oArchivoDetalle.getCodNotaCodificada(),
				oArchivoDetalle.getInicioTdc(),
				oArchivoDetalle.getFinTdc(),
				oArchivoDetalle.getDuracionEjecucion(),
				oArchivoDetalle.getCodCuadrilla(),
				oArchivoDetalle.getObservaciones(), 
				oArchivoDetalle.getNombreAnexo(),
				oArchivoDetalle.getNroCuenta(),
				oArchivoDetalle.getNroMedidor(),
				oArchivoDetalle.getMarcaMedidor(), 
				oArchivoDetalle.getModeloMedidor(),
				oArchivoDetalle.getTipoMedidor(),
				oArchivoDetalle.getAccionMedidor(),
				oArchivoDetalle.getAnioFabricacion(),
				oArchivoDetalle.getUbicacionMedidor(),
				oArchivoDetalle.getLatitud(),
				oArchivoDetalle.getLongitud(),
				oArchivoDetalle.getSeUbicoCliente(),
				oArchivoDetalle.getLectura(),
				oArchivoDetalle.getCondicionVivienda(),
				oArchivoDetalle.getDescripcionPredio(),
				oArchivoDetalle.getNroPisosPredio(),
				oArchivoDetalle.getColorVivienda(),
				oArchivoDetalle.getAledanioIzquierdo(),
				oArchivoDetalle.getAledanioDerecho(),
				oArchivoDetalle.getTieneSuministroAdicional(),
				oArchivoDetalle.getTelefonoCliente(),
				oArchivoDetalle.getTieneLuz(),
				oArchivoDetalle.getTieneMica(),
				oArchivoDetalle.getTieneTapa(),
				oArchivoDetalle.getRecepcionadoPor(),
				oArchivoDetalle.getNroDocumentoCliente(),
				oArchivoDetalle.getTipoDctoIdent(),
				oArchivoDetalle.getFechaEntregaContratista()
                );
	}
	*/
	
	public static void procesarData(String gcTipo)
	{
		/*
		if(!bfnAbrirArchivo())
		{
			CerrarPrograma();
			exit(1);
		}
		
		if(!bfnDeclararCursor())
		{
			CerrarPrograma();
			exit(1);
		}
		
		obtenerDominios();
		bfnObtenerRegistrosProcesar(gcTipo);
		
		log.info("Inicio de impresión de archivo plano...");
		//ifnMsgToLogFile(LOG_DEBUG, LOG_FILE, "Inicio de impresión de archivo plano...");
		
		if(!bfnDeclararCursorImprimir())
		{
			CerrarPrograma();
			exit(1);
		}
		*/
		/*
		while(bfnFetchCursorRegPro())
		{
			iIndicaErr = 0;
			lFilasProc++;
			vLimpiarArreglos();
			
			vCodOperacion=vCodErrASY000;
		    vObservacion=vDesErrASY000;
		    
		    if(!ImprimirPlano())
			{	bfnRollback();
				CerrarPrograma();
				exit(1);
			}
		    
		  //Verificar si la orden tiene anexos
		    vTieneAnexos=vTieneAnexos.trim();
			if (Constantes.debug!=0) log.info("\n vTieneAnexos:[%s]\n",vTieneAnexos);
			if (Constantes.debug!=0) log.info("\n vCodEstadoAnexo:[%d]\n",vCodEstadoAnexo);
			if (Constantes.debug!=0) log.info("\n vCodEstPendRecepAnexos:[%d]\n",vCodEstPendRecepAnexos);
			if (Constantes.debug!=0) log.info("\n gcPathOUT:[%s]\n",gcPathOUT);
			if (Constantes.debug!=0) log.info("\n gcNum_Orden:[%s]\n",gcNum_Orden);
			
			if(iEsReporte == 0){//proceso normal
				
				if(!bfnActualizaRegistro())
		  		{	bfnRollback();
		  		}else{
		  		  bfnCommit();
		  		}
				
			}
			
			if(iIndicaErr==1) {lFilasErr++;}
		}
		
		
		if(lFilasProc==0)
		{	
			//ifnMsgToLogFile(LOG_DEBUG, LOG_FILE | LOG_STDOUT, "NO se encontraron registros a procesar");
			log.info("NO se encontraron registros a procesar");
		}
		
		lFilasCorrectas = lFilasProc - lFilasErr;
		
		log.info("Archivo Procesado: %s", gcNomArchivoOUT);
		log.info("Filas procesadas correctamente : %ld", lFilasCorrectas);
		log.info("Filas procesadas con error     : %ld", lFilasErr);
		log.info("Total de Filas                 : %ld", lFilasProc);
		bfnCommit();
		CerrarPrograma();
			*/
		
	}

	
	/*
	public static void crearExcel() {
		//Crear libro de trabajo en blanco
        Workbook workbook = new HSSFWorkbook();
        
        //Crea hoja nueva
        Sheet sheet = workbook.createSheet("Hoja de datos");
		
        //Por cada línea se crea un arreglo de objetos (Object[])
        Map<String, Object[]> datos = new TreeMap<String, Object[]>();
        datos.put("1", new Object[]{"Identificador", "Nombre", "Apellidos"});
        datos.put("2", new Object[]{1, "María", "Remen"});
        datos.put("3", new Object[]{2, "David", "Allos"});
        datos.put("4", new Object[]{3, "Carlos", "Caritas"});
        datos.put("5", new Object[]{4, "Luisa", "Vitz"});
        
        //Iterar sobre datos para escribir en la hoja
        Set<String> keyset = datos.keySet();
        int numeroRenglon = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(numeroRenglon++);
            Object[] arregloObjetos = datos.get(key);
            int numeroCelda = 0;
            for (Object obj : arregloObjetos) {
                Cell cell = row.createCell(numeroCelda++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }
        
        try {
            //Se genera el documento
            FileOutputStream out = new FileOutputStream(new File("ejemplo.xls"));
            workbook.write(out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
	}
	*/

}

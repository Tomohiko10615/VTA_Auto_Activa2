package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosCorrelativoMoverDTO;

public class DatosCorrelativoMoverMapper implements RowMapper<DatosCorrelativoMoverDTO> {

    @Override
    public DatosCorrelativoMoverDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        long idRuta = rs.getLong("id_ruta");
        String codRuta = rs.getString("cod_ruta");
        DatosCorrelativoMoverDTO datos = new DatosCorrelativoMoverDTO();
        datos.setIdRuta(idRuta);
        datos.setCodRuta(codRuta);
        return datos;
    }
}
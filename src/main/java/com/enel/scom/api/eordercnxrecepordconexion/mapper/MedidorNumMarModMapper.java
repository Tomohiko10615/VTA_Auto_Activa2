package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.MedidorNumMarModDTO;

public class MedidorNumMarModMapper implements RowMapper<MedidorNumMarModDTO> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_MEDIDOR = "select\r\n"
			+ "unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/accionMedidor/text()', d.REG_XML))::text AS accionmedidor,\r\n"
			// R.I. REQSCOM10 24/07/2023 INICIO
			+ "unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/factorMedidor/text()', d.REG_XML))::text AS factormedidor,\r\n"
			// R.I. REQSCOM10 24/07/2023 FIN
			+ "unnest(xpath('/RecepcionarResultadoTDC/datosGestionNuevasConexiones/actasConexion/Fecha_Conexion_Red/text()', d.REG_XML))::text AS fechaconexion,\r\n"
			+ "unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/numeroMedidor/text()', d.REG_XML))::text AS nummedidor,\r\n"
			+ "unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/modeloMedidor/text()', d.REG_XML))::text AS modmedidor,\r\n"
			+ "unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/marcaMedidor/text()', d.REG_XML))::text AS marmedidor\r\n"
			+ "from eor_ord_transfer ot, EOR_ORD_TRANSFER_DET d \r\n"
			+ "WHERE ot.id_ord_transfer = d.id_ord_transfer \r\n"
			+ "and ot.cod_tipo_orden_legacy ='OCNX' \r\n"
			+ "and d.accion = 'RECEPCION'\r\n"
			+ "and ot.nro_orden_lgc_relac = ? "
			;
	
	@Override
	public MedidorNumMarModDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String accionmedidor = rs.getString("accionmedidor");
		// R.I. REQSCOM10 24/07/2023 INICIO
		String factormedidor = rs.getString("factormedidor");
		// R.I. REQSCOM10 24/07/2023 FIN
		String fechaconexion = rs.getString("fechaconexion");
		String nummedidor = rs.getString("nummedidor");
		String modmedidor = rs.getString("modmedidor");
		String marmedidor = rs.getString("marmedidor");
		// R.I. REQSCOM10 24/07/2023 INICIO
		return new MedidorNumMarModDTO(accionmedidor, factormedidor, fechaconexion, nummedidor, modmedidor, marmedidor);
		// R.I. REQSCOM10 24/07/2023 FIN
	}
	
}
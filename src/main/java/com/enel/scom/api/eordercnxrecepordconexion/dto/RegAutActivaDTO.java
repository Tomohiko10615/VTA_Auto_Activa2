package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegAutActivaDTO {

	private Long idPcr;
	private Long idPcrTension;
	private Long idSrvElectrico;
	private Long idDirSumin;
	private Long idSrvVenta;
	private Long idSolSrvEle;
	private Long idAlim;
	private Long idSed;
	private Long idSet;
	private Long idLlave;
	private Long idSumIzq;
	private Long idRutaIzq;
	private Long idSumDer;
	private Long idRutaDer;
	private Long idOrdConex;
	private String nroOrdConex;
	private String codSucEjecutora;
	private Long crearRuta;
	private Long idPtoEntrega;
	private String prop_empalme;
	private String tipoConductor;
	private String capInterruptor;
	private String sisProteccion;
	private String cajaMedicion;
	private String cajaToma;
	private Integer cantMedidor;
	private Long medidorInstallIdMedidor;
	private Long medidorInstallIdMedida;
	private Long medidorRetIdMedidor;
	private Long medidorRetIdMedida;
	
	private Long idSector;
	private Long idZona;
	private String correlativo;
	private Long codCentroOperativo;
	
	
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SegUsuarioMapper implements RowMapper<String> {

	public static final String SQLORACLE_SELECT_FOR_IDUSUARIO = "SELECT"
			+ " ID_USUARIO"
			+ "	FROM SEG_USUARIO"
			+ "	WHERE USERNAME = ?";

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		String idUsuario = rs.getString("ID_USUARIO");
		return idUsuario;
	}

}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrdenConexMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_NROORDENCONEX = "SELECT"
			+ " OCNX.NRO_ORDEN as nroOrdConex"
			+ " FROM ORD_ORDEN OVEN, VTA_SRV_VENTA SVEN, ORD_CONEX OCNX, VTA_SOL_SRV_ELE SELE, NUC_CUENTA nc"
			+ " WHERE"
			+ " OVEN.ID_ORDEN = SVEN.ID_ORD_VENTA"
			+ " AND SVEN.ID_ORD_CONEX = OCNX.ID_ORD_CONEX"
			+ " AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE"
			+ " AND SELE.ID_CUENTA_FAC = NC.ID_CUENTA"
			+ " AND OVEN.NRO_ORDEN = ?"
			+ " AND NC.NRO_CUENTA = ?";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		Long nroOrdConex = rs.getLong("nroOrdConex");
		return nroOrdConex;
	}
	
}

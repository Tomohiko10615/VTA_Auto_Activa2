package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MotivoMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_IDMOTIVO = "SELECT"
			+ " ID_MOTIVO"
			+ "	FROM VTA_MOT_OPE"
			+ "	WHERE CODE = ?";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		Long idMotivo = rs.getLong("ID_MOTIVO");
		return idMotivo;
	}
	
}

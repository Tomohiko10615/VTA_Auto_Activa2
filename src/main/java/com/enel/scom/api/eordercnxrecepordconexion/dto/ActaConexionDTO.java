package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ActaConexionDTO {

	private String idNumeroConexion="";
	private String idCodTipoConexionSol="";
	private String lineaNegocio="";
	private String tipoSolicitud="";
	private String idNumeroSolicitud="";
	private String numeroOrdenServicio="";
	private String fechaConexionRed="";
	private String tipoInstalacion="";
	private String tipoDocumentoPropietario="";
	private String documentoClientePropietario="";
	private String nombreClientePropietario="";
	private String apellidoPaternoPropietario="";
	private String apellidoMaternoPropietario="";
	private String nroMedidCliVecDer="";
	private String idCodMarcaMedidVecDer="";
	private String idCodClienteVecinoDerecho="";
	private String nroMedidCliVecIzq="";
	private String idCodMarcaMedidVecIzq="";
	private String idCodClienteVecinoIzq="";
	private String idCodMunicipio="";
	private String telefonoClientePropietario="";
	private String idCodTipoConexionReal="";
	private String longitud="";
	private String latitud="";
	private String longitudAcometida="";
	private String codTipoRed="";
	private String nombrePersonaAtendio="";
	private String cedulaAtendio="";
	private String cedulaTecnico="";
	private String capacidadInterruptor="";
	private String setr="";
	private String alimentador="";
	private String llave="";
	private String medidaConcentrada="";
	private String numeroMedidor="";
	private String marcaMedidorInstalar="";
	private String medidorRetirado="";
	private String marcaMedidorRetirado="";
	private String estadoMedidorRetirado="";
	private String trabajoAdictionalis="";
	private String cantidadPanos="";
	private String mastil3m="";
	private String mastil6m="";
	private String ampliacionCarga="";
	private String estadoLectura="";
	private String sellosCandadosEncontrado="";
	private String sellosBorneraEncontrado="";
	private String sellosCandadosInstalado="";
	private String sellosBorneraInstalado="";
	private String cuentaAlero="";
	private String potencia="";
	private String noEjecutado="";
	private String codTensionServicio="";
	private String sed="";
	private String sector="";
	private String zona="";
	private String correlativo="";
	private String tipoConductor="";
	private String puntoFisicoConexion="";
	
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SrvElectricoMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_IDSRVELECTRICO = "SELECT"
			+ " SELE.ID_SRV_ELECTRICO as idSrvElectricoXy"
			+ " FROM VTA_ORD_VTA OVEN, ORD_ORDEN OOVEN, VTA_SRV_VENTA SVEN, VTA_SOL_SRV_ELE SELE,"
			+ " NUC_CUENTA NC, EOR_SRV_ELEC_XY XY"
			+ " WHERE 1=1"
			+ " AND OVEN.ID_ORD_VENTA = OOVEN.ID_ORDEN"
			+ " AND OOVEN.ID_TIPO_ORDEN = 5"
			+ " AND OOVEN.ID_EMPRESA = 3"
			+ " AND OVEN.ID_ORD_VENTA = SVEN.ID_ORD_VENTA"
			+ " AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE"
			+ " AND SELE.ID_CUENTA_FAC = NC.ID_CUENTA"
			+ " AND SELE.ID_SRV_ELECTRICO = XY.ID_SERVICIO"
			+ " AND OOVEN.NRO_ORDEN = ?"
			+ " AND NC.NRO_CUENTA = ?";

	public static final String SQLORACLE_SELECT_FOR_IDSRVELECTRICO2 = "SELECT"
			+ " SELE.ID_SRV_ELECTRICO as idSrvElectricoXy"
			+ " FROM VTA_ORD_VTA OVEN, ORD_ORDEN OOVEN, VTA_SRV_VENTA SVEN, VTA_SOL_SRV_ELE SELE, NUC_CUENTA NC"
			+ " WHERE 1=1"
			+ " AND OVEN.ID_ORD_VENTA = OOVEN.ID_ORDEN"
			+ " AND OOVEN.ID_TIPO_ORDEN = 5"
			+ " AND OOVEN.ID_EMPRESA = 3"
			+ " AND OVEN.ID_ORD_VENTA = SVEN.ID_ORD_VENTA"
			+ " AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE"
			+ " AND SELE.ID_CUENTA_FAC = NC.ID_CUENTA"
			+ " AND OOVEN.NRO_ORDEN = ?"
			+ " AND NC.NRO_CUENTA = ?";
	
	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		Long idSrvElectricoXy = rs.getLong("idSrvElectricoXy");
		return idSrvElectricoXy;
	}
		
}

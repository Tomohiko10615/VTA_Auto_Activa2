package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SrvPcrMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_IDTENSION = "SELECT"
			+ " id_tension as idTension"
			+ " FROM srv_pcr"
			+ " WHERE id_pcr = ?";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		Long idTension = rs.getLong("idTension");
		return idTension;
	}
	
}

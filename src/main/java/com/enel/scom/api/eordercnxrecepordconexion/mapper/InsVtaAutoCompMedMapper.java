package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class InsVtaAutoCompMedMapper {

	public static final String SQLORACLE_INSERT_FOR_VTAAUTOCOMPMED = "INSERT INTO vta_auto_comp_med"
			+ " (id_auto_comp_med, id_auto_comp, id_empresa, id_medida, valor_lectura)"
			+ "	VALUES(?, ?, 3, ?, ?)";
	
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ValorAlfaNumericoMapper implements RowMapper<String> {

	public static final String SQLORACLE_SELECT_FOR_VALALFNUMERICO = "SELECT"
			+ " VALOR_ALF"
			+ "	FROM COM_PARAMETROS"
			+ "	WHERE SISTEMA = ?"
			+ "	AND ENTIDAD = ?"
			+ "	AND CODIGO = ?";

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String valorAlf = rs.getString("VALOR_ALF");
		return valorAlf;
	}
	
	
}

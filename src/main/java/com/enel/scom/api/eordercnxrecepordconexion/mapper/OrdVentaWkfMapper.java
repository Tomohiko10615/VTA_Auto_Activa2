package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdVentaWkfDTO;

public class OrdVentaWkfMapper implements RowMapper<OrdVentaWkfDTO> {

	public static final String SQLORACLE_SELECT_FOR_ORDENVENTAWKF = "SELECT"
			+ " word.id_state as idStateOven, word.id_workflow as idWorkFlow"
			+ "	FROM ord_orden ooven, wkf_workflow word"
			+ "	WHERE ooven.id_workflow = word.id_workflow AND ooven.nro_orden = ?"
			+ " and ooven.id_empresa = 3 and ooven.id_tipo_orden = 5";

	public static final String SQLORACLE_SELECT_FOR_ORDENVENTAWKF2 = "SELECT"
			+ " wsol.id_state as idStateOven, wsol.id_workflow  as idWorkFlow"
			+ "	FROM ord_orden osol, wkf_workflow wsol"
			+ "	WHERE osol.id_workflow = wsol.id_workflow"
			+ "	AND osol.id_orden = ("
			+ "	SELECT oven.id_solicitud FROM ord_orden  ooven, vta_ord_vta oven"
			+ "	WHERE ooven.id_orden = oven.id_ord_venta"
			+ "	AND ooven.nro_orden = ? and ooven.id_empresa = 3 and ooven.id_tipo_orden = 5)";
	
	@Override
	public OrdVentaWkfDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String idStateOven = rs.getString("idStateOven");
		Long idWkfOrdVenta = rs.getLong("idWorkFlow");
		return new OrdVentaWkfDTO(idStateOven, idWkfOrdVenta);
	}
	
}

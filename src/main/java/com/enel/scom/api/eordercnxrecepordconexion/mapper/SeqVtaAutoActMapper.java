package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SeqVtaAutoActMapper implements RowMapper<Long> {

	public static final String SQLORACLE_SELECT_FOR_SEQVTAAUTOACT = "SELECT"
			+ " SQVTAAUTOACT.nextval as seqVtaAutoAct"
			+ " FROM dual";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long seqVtaAutoAct = rs.getLong("seqVtaAutoAct");
		return seqVtaAutoAct;
	}
	
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.MedidoresDTO;

public class MedidoresMapper implements RowMapper<MedidoresDTO>{

	public static final String SQLPOSTGRESQL_SELECT_FOR_MEDIDORES = "SELECT\r\n"
			+ " unnest(xpath('/item/numeroMedidor/text()', d.REG_XML))::text AS numeroMedidor,\r\n"
			+ " unnest(xpath('/item/marcaMedidor/text()', d.REG_XML))::text AS marcaMedidor,\r\n"
			+ "	unnest(xpath('/item/modeloMedidor/text()', d.REG_XML))::text AS modeloMedidor,\r\n"
			+ " unnest(xpath('/item/numeroFabricaMedidor/text()', d.REG_XML))::text AS numeroFabricaMedidor,\r\n"
			+ " unnest(xpath('/item/tipoMedidor/text()', d.REG_XML))::text AS tipoMedidor,\r\n"
			+ " unnest(xpath('/item/tecnologiaMedidor/text()', d.REG_XML))::text AS tecnologiaMedidor,\r\n"
			+ "	unnest(xpath('/item/faseMedidor/text()', d.REG_XML))::text AS faseMedidor,\r\n"
			+ " unnest(xpath('/item/factorMedidor/text()', d.REG_XML))::text AS factorMedidor,\r\n"
			+ " unnest(xpath('/item/propiedadMedidor/text()', d.REG_XML))::text AS propiedadMedidor,\r\n"
			+ " unnest(xpath('/item/accionMedidor/text()', d.REG_XML))::text AS accionMedidor,\r\n"
			+ "	unnest(xpath('/item/tipoMedicion/text()', d.REG_XML))::text AS tipoMedicion,\r\n"
			+ " unnest(xpath('/item/ubicacionMedidor/text()', d.REG_XML))::text AS ubicacionMedidor,\r\n"
			+ " unnest(xpath('/item/telemedido/text()', d.REG_XML))::text AS telemedido,\r\n"
			+ "	unnest(xpath('/item/ElementoBorneroPrueba/text()', d.REG_XML))::text AS elementoBorneroPrueba,\r\n"
			+ "	unnest(xpath('/item/AnoFabricacion/text()', d.REG_XML))::text AS anoFabricacion,\r\n"
			+ "	unnest(xpath('/item/fechaInstalacion/text()', d.REG_XML))::text AS fechaInstalacion\r\n"
			+ "FROM (SELECT  unnest(xpath('/medidores/item', unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores', d.REG_XML)))) AS REG_XML  \r\n"
			+ "                    FROM SCHSCOM.EOR_ORD_TRANSFER_DET d  \r\n"
			+ "                    WHERE d.id_ord_transfer =   ?\r\n"
			+ "                    	AND accion = 'RECEPCION'  \r\n"
			+ "                    	AND nro_evento = ?)d ";

	@Override
	public MedidoresDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String numeroMedidor = rs.getString("numeroMedidor");
		String marcaMedidor = rs.getString("marcaMedidor");
		String modeloMedidor = rs.getString("modeloMedidor");
		String numeroFabricaMedidor = rs.getString("numeroFabricaMedidor");
		String tipoMedidor = rs.getString("tipoMedidor");
		String tecnologiaMedidor = rs.getString("tecnologiaMedidor");
		String faseMedidor = rs.getString("faseMedidor");
		String factorMedidor = rs.getString("factorMedidor");
		String propiedadMedidor = rs.getString("propiedadMedidor");
		String accionMedidor = rs.getString("accionMedidor");
		String tipoMedicion = rs.getString("tipoMedicion");
		String ubicacionMedidor = rs.getString("ubicacionMedidor");
		String telemedido = rs.getString("telemedido");
		String elementoBorneroPrueba = rs.getString("elementoBorneroPrueba");
		String anoFabricacion = rs.getString("anoFabricacion");
		String fechaInstalacion = rs.getString("fechaInstalacion");
		return new MedidoresDTO(numeroMedidor, marcaMedidor, modeloMedidor, numeroFabricaMedidor, tipoMedidor, tecnologiaMedidor, faseMedidor, factorMedidor, propiedadMedidor,
				accionMedidor, tipoMedicion, ubicacionMedidor, telemedido, elementoBorneroPrueba, anoFabricacion, fechaInstalacion);
	}
	
	
    
}

package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class NucEmpresaMapper implements RowMapper<Long>{

	public static final String SQLORACLE_SELECT_FOR_IDEMPRESA = "SELECT"
			+ " ID_EMPRESA"
			+ "	FROM NUC_EMPRESA"
			+ "	WHERE COD_PARTITION = ?";

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idEmpresa = rs.getLong("ID_EMPRESA");
		return idEmpresa;
	}
	
}

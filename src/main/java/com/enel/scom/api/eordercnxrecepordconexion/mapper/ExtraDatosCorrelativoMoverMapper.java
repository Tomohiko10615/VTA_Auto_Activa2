package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.DatosCorrelativoMoverDTO;

public class ExtraDatosCorrelativoMoverMapper implements RowMapper<DatosCorrelativoMoverDTO> {

    @Override
    public DatosCorrelativoMoverDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        Long idServVenta = rs.getLong("idSrvVenta");
        Long idSector = rs.getLong("idSectorLec");
        Long idZona = rs.getLong("idZonaLec");
        String correl = rs.getString("corrLec");

        DatosCorrelativoMoverDTO datos = new DatosCorrelativoMoverDTO();
        datos.setIdServVenta(idServVenta);
        datos.setIdSector(idSector);
        datos.setIdZona(idZona);
        datos.setCorrel(correl);

        return datos;
    }
}

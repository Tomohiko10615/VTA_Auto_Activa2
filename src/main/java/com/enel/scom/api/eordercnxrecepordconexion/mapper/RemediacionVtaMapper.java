package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.RemediacionVtaDTO;

public class RemediacionVtaMapper implements RowMapper<RemediacionVtaDTO>{

	public static final String SQLORACLE_SELECT_FOR_REMEDIACIONVTA = "SELECT"
			+ " DECODE(NVL( "
			+ "	("
			+ "	SELECT COUNT(*)"
			+ "	FROM NUC_CONTRATO CONT2, VTA_SRV_VENTA SVEN2, VTA_SOL_SRV_ELE SELE2, VTA_CONTRATO VCON2,"
			+ "	PRD_PRODUCTO PROD2, PRD_PROPIEDAD PROP2, NUC_SERVICIO NS2, NUC_CUENTA NC2 "
			+ "	WHERE SVEN2.ID_ORD_VENTA = ?"
			+ "	AND SVEN2.ID_SRV_VENTA = ?"
			+ "	AND SELE2.ID_SRV_ELECTRICO = NS2.ID_SERVICIO"
			+ "	AND NS2.ID_CUENTA = NC2.ID_CUENTA"
			+ "	AND SVEN2.ID_SOL_SRV_ELE = SELE2.ID_SOL_SRV_ELE"
			+ "	AND SELE2.ID_CATEG_TARIFA IS NOT NULL"
			+ "	AND SELE2.ID_TARIFA IS NOT NULL"
			+ "	AND SVEN2.ID_PRODUCTO = PROD2.ID_PRODUCTO"
			+ "	AND PROD2.ID_PRODUCTO = PROP2.ID_PRODUCTO"
			+ "	AND PROP2.TIP_PROPIEDAD = 'CON'"
			+ "	AND PROP2.VAL_PROPIEDAD = 'S'"
			+ "	AND ( (SVEN2.ID_SRV_VENTA = CONT2.ID_COMERCIAL"
			+ "	AND CONT2.TYPE_COMERCIAL = 'com.synapsis.synergia.vta.domain.ServicioVenta')"
			+ "	OR (SELE2.ID_SRV_ELECTRICO = CONT2.ID_COMERCIAL"
			+ "	AND CONT2.TYPE_COMERCIAL = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico')"
			+ "	)"
			+ "	AND VCON2.ID_CONTRATO = CONT2.ID_CONTRATO"
			+ "	AND VCON2.EMITIDO = 'S'"
			+ "	)"
			+ "	,0), 0, 'NO', 'SI') tieneContrato,"
			+ "	DECODE(NVL("
			+ "	("
			+ "	SELECT COUNT(*)"
			+ "	FROM VTA_SRV_VENTA SVEN2, VTA_SOL_SRV_ELE SELE2, UBI_DIRECCION DIR1, UBI_DIRECCION DIR2, SRV_ELECTRICO SRVE2,"
			+ "	SRV_ELECT_ESTADO SEST, PRD_PRODUCTO PROD2"
			+ "	WHERE SVEN2.ID_ORD_VENTA = ?"
			+ "	AND SVEN2.ID_SRV_VENTA = ?"
			+ "	AND (SVEN2.ESTADO='Ejecutado' OR"
			+ "	SVEN2.ESTADO='EnProcesoDeHabilitar'OR SVEN2.ESTADO='EsperaRtaCliente'OR SVEN2.ESTADO='Generado' OR"
			+ "	SVEN2.ESTADO='Inspeccionado'OR SVEN2.ESTADO='NoEjecutado' OR SVEN2.ESTADO='Habilitado') AND"
			+ "	SVEN2.ID_SOL_SRV_ELE=SELE2.ID_SOL_SRV_ELE AND"
			+ "	SVEN2.ID_PRODUCTO=PROD2.ID_PRODUCTO AND (PROD2.TIP_PRODUCTO='NVO' OR PROD2.TIP_PRODUCTO='REI') AND"
			+ "	SELE2.ID_DIR_SERVICIO=DIR1.ID_DIRECCION AND SRVE2.ID_DIRECCION=DIR2.ID_DIRECCION AND DIR1.DIRECCION=DIR2.DIRECCION AND"
			+ "	SRVE2.ID_ESTADO =SEST.ID_ESTADO AND SEST.COD_INTERNO='Habilitado'"
			+ "	)"
			+ "	,0) , 0, 'NO', 'SI') tieneDirRepetidas,"
			+ "	DECODE(NVL("
			+ "	("
			+ "	SELECT COUNT(*)"
			+ "	FROM VTA_SRV_VENTA SVEN2, VTA_SOL_SRV_ELE SELE2, UBI_DIRECCION DIR1, PRD_PRODUCTO PROD2,"
			+ "	VTA_SRV_VENTA SVEN3, VTA_SOL_SRV_ELE SELE3, UBI_DIRECCION DIR3, PRD_PRODUCTO PROD3"
			+ "	WHERE"
			+ "	SVEN2.ID_ORD_VENTA = ?"
			+ "	AND SVEN2.ID_SRV_VENTA = ?"
			+ "	AND (SVEN2.ESTADO='Ejecutado' OR"
			+ "	SVEN2.ESTADO='EnProcesoDeHabilitar'OR SVEN2.ESTADO='EsperaRtaCliente'OR SVEN2.ESTADO='Generado' OR"
			+ "	SVEN2.ESTADO='Inspeccionado'OR SVEN2.ESTADO='NoEjecutado' OR SVEN2.ESTADO='Habilitado')"
			+ "	AND SVEN2.ID_SOL_SRV_ELE = SELE2.ID_SOL_SRV_ELE"
			+ "	AND SVEN2.ID_PRODUCTO = PROD2.ID_PRODUCTO"
			+ "	AND (PROD2.TIP_PRODUCTO='NVO' OR PROD2.TIP_PRODUCTO='REI')"
			+ "	AND SELE2.ID_DIR_SERVICIO=DIR1.ID_DIRECCION"
			+ "	AND SVEN3.ID_ORD_VENTA = ?"
			+ "	AND (SVEN3.ESTADO='Ejecutado' OR"
			+ "	SVEN3.ESTADO='EnProcesoDeHabilitar'OR SVEN3.ESTADO='EsperaRtaCliente'OR SVEN3.ESTADO='Generado'OR"
			+ "	SVEN3.ESTADO='Inspeccionado'OR SVEN3.ESTADO='NoEjecutado' OR SVEN3.ESTADO='Habilitado')"
			+ "	AND SVEN3.ID_SOL_SRV_ELE = SELE3.ID_SOL_SRV_ELE"
			+ "	AND SVEN3.ID_PRODUCTO = PROD3.ID_PRODUCTO"
			+ "	AND (PROD3.TIP_PRODUCTO='NVO' OR PROD3.TIP_PRODUCTO='REI')"
			+ "	AND SELE3.ID_DIR_SERVICIO=DIR3.ID_DIRECCION"
			+ "	AND DIR1.DIRECCION = DIR3.DIRECCION"
			+ "	AND SVEN3.ID_SRV_VENTA <> SVEN2.ID_SRV_VENTA"
			+ "	)"
			+ " ,0) , 0, 'NO', 'SI') tieneDirRepetidasIn"
			+ " FROM DUAL";

	@Override
	public RemediacionVtaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String tieneContrato = rs.getString("tieneContrato");
		String tieneDirRepetidas = rs.getString("tieneDirRepetidas");
		String tieneDirRepetidasIn = rs.getString("tieneDirRepetidasIn");
		return new RemediacionVtaDTO(tieneContrato, tieneDirRepetidas, tieneDirRepetidasIn);
	}  
	  
}

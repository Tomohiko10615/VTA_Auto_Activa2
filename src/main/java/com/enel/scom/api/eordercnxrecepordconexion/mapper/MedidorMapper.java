// R.I. REQSCOM10 21/07/2023 INICIO
package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class MedidorMapper {
	
	private MedidorMapper() {
		super();
	}

	public static final String SQLPOSTGRESQL_UPDATE_FACTOR = "UPDATE med_medida_medidor AS mmm\r\n"
			+ "SET id_factor = mfm.id_factor,\r\n"
			+ "    val_factor = mf.val_factor\r\n"
			+ "FROM med_medida_modelo AS mmm1\r\n"
			+ "JOIN med_fac_med_mod AS mfm ON mmm1.id = mfm.id_medida_modelo\r\n"
			+ "JOIN med_factor AS mf ON mfm.id_factor = mf.id\r\n"
			+ "WHERE mmm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar\r\n"
			+ " where mc.id_modelo = mmod.id \r\n"
			+ "  and mmod.id_marca = mmar.id \r\n"
			+ " and mc.nro_componente = ? \r\n"
			+ "\r\n"
			+ " and mmod.cod_modelo = ?\r\n"
			+ " and mmar.cod_marca = ?)\r\n"
			+ "    AND mmm1.id_modelo = (select mmod.id from med_modelo mmod, med_marca mmar\r\n"
			+ "         where mmod.cod_modelo = ? and mmar.cod_marca = ? and mmod.id_marca = mmar.id)\r\n"
			+ "    AND mf.cod_factor = ?\r\n"
			+ "    AND mmm.id_medida = mmm1.id_medida -- Actualiza por cada medida\r\n"
			+ "    AND NOT EXISTS ( -- Verifica que no exista un registro en med_medida_medidor con el nuevo factor no válido\r\n"
			+ "        SELECT 1\r\n"
			+ "        FROM med_medida_medidor AS mm\r\n"
			+ "        WHERE mm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar\r\n"
			+ "where mc.id_modelo = mmod.id\r\n"
			+ "and mmod.id_marca = mmar.id\r\n"
			+ " and mc.nro_componente = ? \r\n"
			+ "and mmod.cod_modelo = ?\r\n"
			+ "and mmar.cod_marca = ?)\r\n"
			+ "        AND NOT EXISTS (\r\n"
			+ "            SELECT 1\r\n"
			+ "            FROM med_medida_modelo AS mmm2\r\n"
			+ "            JOIN med_fac_med_mod AS mfm2 ON mmm2.id = mfm2.id_medida_modelo\r\n"
			+ "            JOIN med_factor AS mf2 ON mfm2.id_factor = mf2.id\r\n"
			+ "            WHERE mmm2.id_modelo = (select mmod.id from med_modelo mmod, med_marca mmar\r\n"
			+ "          where mmod.cod_modelo = ? and mmar.cod_marca = ? and mmod.id_marca = mmar.id)\r\n"
			+ "            AND mf2.cod_factor = ?\r\n"
			+ "            AND mm.id_medida = mmm2.id_medida\r\n"
			+ "        )\r\n"
			+ "    )";
	
	public static final String SQLPOSTGRESQL_SELECT_COUNT_FACTOR = "SELECT COUNT(*) FROM med_medida_medidor AS mmm,\r\n"
			+ "med_medida_modelo AS mmm1 \r\n"
			+ "JOIN med_fac_med_mod AS mfm ON mmm1.id = mfm.id_medida_modelo\r\n"
			+ "JOIN med_factor AS mf ON mfm.id_factor = mf.id\r\n"
			+ "WHERE mmm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar\r\n"
			+ " where mc.id_modelo = mmod.id \r\n"
			+ "  and mmod.id_marca = mmar.id \r\n"
			+ " and mc.nro_componente = ? \r\n"
			+ "\r\n"
			+ " and mmod.cod_modelo = ?\r\n"
			+ " and mmar.cod_marca = ?)\r\n"
			+ "    AND mmm1.id_modelo = (select mmod.id from med_modelo mmod, med_marca mmar\r\n"
			+ "         where mmod.cod_modelo = ? and mmar.cod_marca = ? and mmod.id_marca = mmar.id)\r\n"
			+ "    AND mf.cod_factor = ?\r\n"
			+ "    AND mmm.id_medida = mmm1.id_medida -- Actualiza por cada medida\r\n"
			+ "    AND NOT EXISTS ( -- Verifica que no exista un registro en med_medida_medidor con el nuevo factor no válido\r\n"
			+ "        SELECT 1\r\n"
			+ "        FROM med_medida_medidor AS mm\r\n"
			+ "        WHERE mm.id_componente = (select mc.id from med_componente mc, med_modelo mmod, med_marca mmar\r\n"
			+ "where mc.id_modelo = mmod.id\r\n"
			+ "and mmod.id_marca = mmar.id\r\n"
			+ " and mc.nro_componente = ? \r\n"
			+ "and mmod.cod_modelo = ?\r\n"
			+ "and mmar.cod_marca = ?)\r\n"
			+ "        AND NOT EXISTS (\r\n"
			+ "            SELECT 1\r\n"
			+ "            FROM med_medida_modelo AS mmm2\r\n"
			+ "            JOIN med_fac_med_mod AS mfm2 ON mmm2.id = mfm2.id_medida_modelo\r\n"
			+ "            JOIN med_factor AS mf2 ON mfm2.id_factor = mf2.id\r\n"
			+ "            WHERE mmm2.id_modelo = (select mmod.id from med_modelo mmod, med_marca mmar\r\n"
			+ "          where mmod.cod_modelo = ? and mmar.cod_marca = ? and mmod.id_marca = mmar.id)\r\n"
			+ "            AND mf2.cod_factor = ?\r\n"
			+ "            AND mm.id_medida = mmm2.id_medida\r\n"
			+ "        )\r\n"
			+ "    )";

}
//R.I. REQSCOM10 21/07/2023 FIN
package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.VtaAutoActDTO;

public class VtaAutoActMapper implements RowMapper<VtaAutoActDTO> {

	public static final String SQLORACLE_SELECT_FOR_VTAAUTOACT = "SELECT"
			+ " a.id_auto_act as idAutoAct, nro_cuenta as nroCuenta"
			+ " FROM vta_auto_act a, vta_auto_dt b, vta_srv_venta c,"
			+ " vta_sol_srv_ele d, nuc_cuenta e"
			+ " WHERE a.id_auto_act=b.id_auto_act"
			+ " AND b.id_srv_venta=c.id_srv_venta"
			+ " AND c.id_sol_srv_ele = d.id_sol_srv_ele"
			+ " AND d.id_cuenta_fac=e.id_cuenta"
			+ " AND a.nro_orden= ?"
			+ " AND c.estado = 'Ejecutado'"
			+ " ORDER BY a.id_auto_act desc";

	@Override
	public VtaAutoActDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idAutoAct = rs.getLong("idAutoAct");
		Long nroCuenta = rs.getLong("nroCuenta");
		return new VtaAutoActDTO(idAutoAct, nroCuenta);
	}
	
	
}

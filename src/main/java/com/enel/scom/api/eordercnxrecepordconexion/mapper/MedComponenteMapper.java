package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.FwkAuditEventDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.MedComponenteDTO;

public class MedComponenteMapper implements RowMapper<MedComponenteDTO> {

	public static final String SQLORACLESQL_SELECT_FOR_MED_COMPONENTE = "SELECT  "
			+ "ID_COMPONENTE     ,\r\n"
			+ "ID_EMPRESA               ,\r\n"
			+ "ID_MODELO                ,\r\n"
			+ "ID_EST_COMPONENTE        ,\r\n"
			+ "FEC_CREACION             ,\r\n"
			+ "ID_PROPIEDAD             ,\r\n"
			+ "ANO_FABRICACION          ,\r\n"
			+ "NRO_FABRICA              ,\r\n"
			+ "ID_ACCION_REALIZADA      ,\r\n"
			+ "ID_COND_CREACION         ,\r\n"
			+ "ES_CONTROL_INTERNO       ,\r\n"
			+ "ES_PROTOTIPO             ,\r\n"
			+ "ID_TIP_TRANSFOR          ,\r\n"
			+ "COD_TIP_COMPONENTE       ,\r\n"
			+ "ID_EQUIPO                ,\r\n"
			+ "ID_RESP_MEDICION         ,\r\n"
			+ "ID_MOD_MEDICION          ,\r\n"
			+ "TEL_MEDICION             ,\r\n"
			+ "UBI_INSTALACION          ,\r\n"
			+ "ID_UBICACION             ,\r\n"
			+ "TYPE_UBICACION           ,\r\n"
			+ "ID_DYNAMICOBJECT         ,\r\n"
			+ "HORA_RELOJ               ,\r\n"
			+ "HORA_VERIF               ,\r\n"
			+ "ID_RELACION              ,\r\n"
			+ "ID_NEV                   ,\r\n"
			+ "ID_TIP_MEDIDA            ,\r\n"
			+ "NRO_COMPONENTE           ,\r\n"
			+ "UBICACION_INST_MEDIDOR   ,\r\n"
			+ "FECHA_SUMINISTRO         ,\r\n"
			+ "SUMINISTRADOR_COMPONENTE ,\r\n"
			+ "REACONDICIONADO          ,\r\n"
			+ "ID_MOTIVO_BAJA           ,\r\n"
			+ "FACTOR_INTERNO           ,\r\n"
			+ "SERIAL_NUMBER\r\n"
			+ "FROM med_componente\r\n"
			+ "WHERE ID_COMPONENTE  = ? "
			;
	
	public static final String SQLPOSTGREEQL_UPDATE_FOR_MED_COMPONENTE = "update schscom.med_componente\r\n"
			+ "set \r\n"
			+ "id_modelo               = ? ,\r\n"
			+ "id_est_componente       = ? ,\r\n"
			+ "fec_creacion            = ?::timestamp ,\r\n"
			+ "id_propiedad            = ? ,\r\n"
			+ "ano_fabricacion         = ? ,\r\n"
			+ "nro_fabrica             = ? ,\r\n"
			+ "id_accion_realizada     = ? ,\r\n"
			+ "id_cond_creacion        = ? ,\r\n"
			+ "es_control_interno      = ? ,\r\n"
			+ "es_prototipo            = ? ,\r\n"
			+ "id_tip_transfor         = ? ,\r\n"
			+ "cod_tip_componente      = ? ,\r\n"
			+ "id_equipo               = ? ,\r\n"
			+ "id_resp_medicion        = ? ,\r\n"
			+ "id_mod_medicion         = ? ,\r\n"
			+ "tel_medicion            = ? ,\r\n"
			+ "ubi_instalacion         = ? ,\r\n"
			+ "id_ubicacion            = ? ,\r\n"
			+ "type_ubicacion          = ? ,\r\n"
			+ "id_dynamicobject        = ? ,\r\n"
			+ "hora_reloj              = ? ,\r\n"
			+ "hora_verif              = ? ,\r\n"
			+ "id_relacion             = ? ,\r\n"
			+ "id_nev                  = ? ,\r\n"
			+ "id_tip_medida           = ? ,\r\n"
			+ "nro_componente          = ? ,\r\n"
			+ "ubicacion_inst_medidor  = ? ,\r\n"
			+ "fecha_suministro        = ?::timestamp ,\r\n"
			+ "suministrador_componente= ? ,\r\n"
			+ "reacondicionado         = ? ,\r\n"
			+ "id_motivo_baja          = ? ,\r\n"
			+ "factor_interno          = ? ,\r\n"
			+ "serial_number           = ? \r\n"
//			+ "id_usuario_registro     = ? ,\r\n"
//			+ "fec_registro            = ? ,\r\n"
//			+ "id_usuario_modif        = ? ,\r\n"
//			+ "fec_modif               = ? ,\r\n"
//			+ "activo                  = ? ,\r\n"
//			+ "serie                   = ?  \r\n"
			+ "where id = ?"
			;
	
			@Override
			public MedComponenteDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

				Long ID_COMPONENTE = rs.getLong("ID_COMPONENTE");
				Long ID_EMPRESA = rs.getLong("ID_EMPRESA");
				Long ID_MODELO = rs.getLong("ID_MODELO");
				Long ID_EST_COMPONENTE = rs.getLong("ID_EST_COMPONENTE");
				String FEC_CREACION = rs.getString("FEC_CREACION");
				Long ID_PROPIEDAD = rs.getLong("ID_PROPIEDAD");
				Long ANO_FABRICACION = rs.getLong("ANO_FABRICACION");
				Long NRO_FABRICA = rs.getLong("NRO_FABRICA");
				if(rs.getObject("NRO_FABRICA") == null) {
					NRO_FABRICA = null;
				}
				Long ID_ACCION_REALIZADA = rs.getLong("ID_ACCION_REALIZADA");
				if(rs.getObject("ID_ACCION_REALIZADA") == null) {
					ID_ACCION_REALIZADA = null;
				}
				Long ID_COND_CREACION = rs.getLong("ID_COND_CREACION");
				String ES_CONTROL_INTERNO = rs.getString("ES_CONTROL_INTERNO");
				String ES_PROTOTIPO = rs.getString("ES_PROTOTIPO");
				Long ID_TIP_TRANSFOR = rs.getLong("ID_TIP_TRANSFOR");
				if(rs.getObject("ID_TIP_TRANSFOR") == null) {
					ID_TIP_TRANSFOR = null;
				}
				String COD_TIP_COMPONENTE = rs.getString("COD_TIP_COMPONENTE");
				Long ID_EQUIPO = rs.getLong("ID_EQUIPO");
				if(rs.getObject("ID_EQUIPO") == null) {
					ID_EQUIPO = null;
				}
				Long ID_RESP_MEDICION = rs.getLong("ID_RESP_MEDICION");
				if(rs.getObject("ID_RESP_MEDICION") == null) {
					ID_RESP_MEDICION = null;
				}
				Long ID_MOD_MEDICION = rs.getLong("ID_MOD_MEDICION");
				if(rs.getObject("ID_MOD_MEDICION") == null) {
					ID_MOD_MEDICION = null;
				}
				String TEL_MEDICION = rs.getString("TEL_MEDICION");
				if(rs.getObject("TEL_MEDICION") == null) {
					TEL_MEDICION = null;
				}
				String UBI_INSTALACION = rs.getString("UBI_INSTALACION");
				if(rs.getObject("UBI_INSTALACION") == null) {
					UBI_INSTALACION = null;
				}
				Long ID_UBICACION = rs.getLong("ID_UBICACION");
				String TYPE_UBICACION = rs.getString("TYPE_UBICACION");
				Long ID_DYNAMICOBJECT = rs.getLong("ID_DYNAMICOBJECT");
				String HORA_RELOJ = rs.getString("HORA_RELOJ");
				String HORA_VERIF = rs.getString("HORA_VERIF");
				Long ID_RELACION = rs.getLong("ID_RELACION");
				if(rs.getObject("ID_RELACION") == null) {
					ID_RELACION = null;
				}
				Long ID_NEV = rs.getLong("ID_NEV");
				if(rs.getObject("ID_NEV") == null) {
					ID_NEV = null;
				}
				Long ID_TIP_MEDIDA = rs.getLong("ID_TIP_MEDIDA");
				if(rs.getObject("ID_TIP_MEDIDA") == null) {
					ID_TIP_MEDIDA = null;
				}
				String NRO_COMPONENTE = rs.getString("NRO_COMPONENTE");
				String UBICACION_INST_MEDIDOR = rs.getString("UBICACION_INST_MEDIDOR");
				String FECHA_SUMINISTRO = rs.getString("FECHA_SUMINISTRO");
				String SUMINISTRADOR_COMPONENTE = rs.getString("SUMINISTRADOR_COMPONENTE");
				String REACONDICIONADO = rs.getString("REACONDICIONADO");
				Long ID_MOTIVO_BAJA = rs.getLong("ID_MOTIVO_BAJA");
				if(rs.getObject("ID_MOTIVO_BAJA") == null) {
					ID_MOTIVO_BAJA = null;
				}
				Long FACTOR_INTERNO = rs.getLong("FACTOR_INTERNO");
				if(rs.getObject("FACTOR_INTERNO") == null) {
					FACTOR_INTERNO = null;
				}
				String SERIAL_NUMBER = rs.getString("SERIAL_NUMBER");

				return new MedComponenteDTO(ID_COMPONENTE, ID_EMPRESA, ID_MODELO, ID_EST_COMPONENTE, FEC_CREACION,
						ID_PROPIEDAD, ANO_FABRICACION, NRO_FABRICA, ID_ACCION_REALIZADA, ID_COND_CREACION,
						ES_CONTROL_INTERNO, ES_PROTOTIPO, ID_TIP_TRANSFOR, COD_TIP_COMPONENTE, ID_EQUIPO,
						ID_RESP_MEDICION, ID_MOD_MEDICION, TEL_MEDICION, UBI_INSTALACION, ID_UBICACION, TYPE_UBICACION,
						ID_DYNAMICOBJECT, HORA_RELOJ, HORA_VERIF, ID_RELACION, ID_NEV, ID_TIP_MEDIDA, NRO_COMPONENTE,
						UBICACION_INST_MEDIDOR, FECHA_SUMINISTRO, SUMINISTRADOR_COMPONENTE, REACONDICIONADO,
						ID_MOTIVO_BAJA, FACTOR_INTERNO, SERIAL_NUMBER);
			}
	
}
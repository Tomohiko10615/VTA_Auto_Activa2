package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.DireccionOrdenDTO;

public class DireccionOrdenMapper implements RowMapper<DireccionOrdenDTO>{
	
	public static final String SQLORACLE_SELECT_FOR_DIRECCIONORDEN ="SELECT atributo\r\n"
			+ "        ,valor\r\n"
			+ "        ,des_valor\r\n"
			+ "    FROM (\r\n"
			+ "        SELECT TRIM(UPPER(NVL(UBVD.ATRIBUTO, ''))) atributo\r\n"
			+ "            ,TRIM(UPPER(NVL(UBVD.VALOR, ''))) valor\r\n"
			+ "            ,(\r\n"
			+ "                SELECT TRIM(UPPER(NVL(DES_VALOR, '')))\r\n"
			+ "                FROM UBI_LISTA_VALOR_DIR\r\n"
			+ "                WHERE ID_LISTA_VALOR_DIR = UBVD.ID_LISTA_VALOR_DIR\r\n"
			+ "                ) des_valor\r\n"
			+ "        FROM VTA_SOLICITUD SOL\r\n"
			+ "            ,ORD_ORDEN OSOL\r\n"
			+ "            ,VTA_ORD_VTA OVEN\r\n"
			+ "            ,ORD_ORDEN OOVEN\r\n"
			+ "            ,VTA_SRV_VENTA SVEN\r\n"
			+ "            ,VTA_SOL_SRV_ELE SELE\r\n"
			+ "            ,UBI_VALOR_DIR UBVD\r\n"
			+ "        WHERE SOL.ID_SOLICITUD = OSOL.ID_ORDEN\r\n"
			+ "            AND OSOL.ID_TIPO_ORDEN = 4\r\n"
			+ "            AND OSOL.ID_EMPRESA = 3\r\n"
			+ "            AND SOL.ID_SOLICITUD = OVEN.ID_SOLICITUD\r\n"
			+ "            AND OVEN.ID_ORD_VENTA = OOVEN.ID_ORDEN\r\n"
			+ "            AND OOVEN.ID_TIPO_ORDEN = 5\r\n"
			+ "            AND OOVEN.ID_EMPRESA = 3\r\n"
			+ "            AND OVEN.ID_ORD_VENTA = SVEN.ID_ORD_VENTA\r\n"
			+ "            AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE\r\n"
			+ "            AND UBVD.ID_DIRECCION = SELE.ID_DIR_SERVICIO\r\n"
			+ "            AND OOVEN.NRO_ORDEN = ?\r\n"
			+ "        ) atributos\r\n"
			+ "    GROUP BY atributo ,valor ,des_valor";
	
	@Override
	public DireccionOrdenDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String atributo = rs.getString("atributo");
		String valor = rs.getString("valor");
		String des_valor = rs.getString("des_valor");
		return new DireccionOrdenDTO(atributo,valor,des_valor);
	}
}

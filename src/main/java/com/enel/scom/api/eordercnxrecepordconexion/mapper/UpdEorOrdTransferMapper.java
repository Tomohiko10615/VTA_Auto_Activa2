package com.enel.scom.api.eordercnxrecepordconexion.mapper;

public class UpdEorOrdTransferMapper {

	public static final String SQLPOSTGRE_UPDATE_FOR_SUSPEORORDTRANSFER = "UPDATE"
			+ " schscom.eor_ord_transfer"
			+ "	SET suspendida = 'S',"
			+ "	COD_ESTADO_ORDEN = ?,"
			+ "	COD_OPERACION = ?,"
			+ " COD_ESTADO_ORDEN_ANT=COD_ESTADO_ORDEN,"
			+ "	OBSERVACIONES = ''"
			+ "	WHERE ID_ORD_TRANSFER = ?";
	
	public static final String SQLPOSTGRE_UPDATE_FOR_ERRORORDTRANSFER = "UPDATE"
			+ " schscom.eor_ord_transfer"
			+ "	SET fec_operacion = now(),"
			+ " COD_OPERACION = ?,"
			+ " OBSERVACIONES = ?,"
			+ " COD_ESTADO_ORDEN_ANT=COD_ESTADO_ORDEN,"
			+ " COD_ESTADO_ORDEN = ("
			+ "	CASE WHEN ? = ? THEN ?"
			+ "	ELSE ?"
			+ "	END )"
			+ "	WHERE ID_ORD_TRANSFER = ?";
	
	public static final String SQLPOSTGRE_UPDATE_FOR_EORORDTRANSFER = "UPDATE"
			+ " schscom.eor_ord_transfer"
			+ " SET fec_operacion = now(),"
			+ " COD_OPERACION = ?,"
			+ " OBSERVACIONES = ?,"
			+ " COD_ESTADO_ORDEN_ANT=COD_ESTADO_ORDEN,"
			+ " COD_ESTADO_ORDEN = ("
			+ "	CASE WHEN ? = ? THEN ? WHEN ? = ? THEN ?"
			+ "	ELSE ?"
			+ "	END )"
			+ "	WHERE ID_ORD_TRANSFER = ?";
}

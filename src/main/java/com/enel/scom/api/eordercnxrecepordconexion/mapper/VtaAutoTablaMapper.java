package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.VtaAutoTablaDTO;

public class VtaAutoTablaMapper implements RowMapper<VtaAutoTablaDTO> {

	public static final String SQLORACLE_SELECT_FOR_VTAAUTOTABLA = "SELECT"
			+ " codigo,'' descripDTC"
			+ " FROM vta_auto_tabla"
			+ " WHERE upper(descripcion) = upper(?)"
			+ " AND tabla= ?";
	
	public static final String SQLORACLE_SELECT_FOR_DESCRIPDTC = "SELECT '' codigo,"
			+ " descripcion as descripDTC"
			+ "	FROM vta_auto_tabla"
			+ "	WHERE codigo = ?"
//			+ "	WHERE descripcion = ?"
			+ "	AND tabla= ?";

	@Override
	public VtaAutoTablaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {		
		VtaAutoTablaDTO oVtaAutoTablaDTO= new VtaAutoTablaDTO();
		oVtaAutoTablaDTO.setCodigo(rs.getString("codigo"));
		oVtaAutoTablaDTO.setDescripcion(rs.getString("descripDTC"));
		return oVtaAutoTablaDTO;
	}
	
}

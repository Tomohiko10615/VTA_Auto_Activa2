package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.CargaTagDTO;

public class CargaTagMapper implements RowMapper<CargaTagDTO> {

	public static final String SQLORACLE_SELECT_FOR_CARGATAG = "SELECT"
			+ " lower(A.NOM_TAG) as nomTag, A.NOM_COLUMNA as nomColumna, A.EDELNOR as edelnor, A.OBLIGATORIO as obligatorio, A.ENARCHIVO as enArchivo, "
			+ " A.posicion as posicion ,1, a.POSARCHIVO,A.TIPO"
			+ " FROM EOR_PLANILLA A"
			+ " WHERE A.COD_PROCESO = 'COMUN'"
			+ " AND A.ACTIVO = 'S' AND A.COD_TIPO = 'FR'"
			+ " UNION SELECT lower(A.NOM_TAG), A.NOM_COLUMNA, A.EDELNOR, A.OBLIGATORIO , A.ENARCHIVO, A.posicion,2, a.POSARCHIVO,A.TIPO"
			+ " FROM EOR_PLANILLA A"
			+ " WHERE A.COD_PROCESO = 'CNX' AND A.COD_PLANILLA = 'R002'"
			+ " AND A.ACTIVO = 'S' AND A.COD_TIPO = 'FR'"
			+ " ORDER BY 7,8";

	@Override
	public CargaTagDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		String nomTag = rs.getString("nomTag");
		String nomColumna = rs.getString("nomColumna");
		String edelnor = rs.getString("edelnor");
		String obligatorio = rs.getString("obligatorio");
		String enArchivo = rs.getString("enArchivo");
		Integer posicion = rs.getInt("posicion");
		Integer indQuery =  rs.getInt("indQuery");
		Integer posArchivo = rs.getInt("posArchivo");
		String tipo = rs.getString("tipo");
		return new CargaTagDTO(nomTag, nomColumna, edelnor, obligatorio, enArchivo, posicion, indQuery, posArchivo, tipo);
	}
	
}

package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class RegistroOrdenDTO {

	private String numOrden;
	private String numCuenta;
	private String ptoEntrega; 
	private String propEmpalme;
	private String tipoConductor;
	private String capInterruptor;
	private String sisProteccion;
	private String cajaMedicion;
	private String cajaToma;
	private String acometida;
	private String sumDerecho;
	private String sumIzquierdo;
	private String numPcr;
	private String tensionPcr;
	private String sumPcrRef;
	private String numMedidorInstall;
	private String codMarcaInstall;
	private String codModeloInstall;
	private String fecLecturaInstall;
	private String novedadInstall;
	private String lecturaInstall; 
	private String numMedidorRet;
	private String codMarcaRet;
	private String codModeloRet;
	private String fecLecturaRet;
	private String novedadRet;
	private String lecturaRet;
	private String set;
	private String alimentador;
	private String sed; 
	private String llave;
	private String sector;
	private String zona;
	private String correlativo;
	private String fechaEjecucion;
	private String latitudMedidor;
	private String longitudMedidor;
	private String latitudEmpalme;
	private String longitudEmpalme;
	
	private String codFactor; // R.I. Agregado 01/11/2023
}

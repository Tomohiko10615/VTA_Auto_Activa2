package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.EorOrdTransferDTO;

public class EorOrdTransferMapper implements RowMapper<EorOrdTransferDTO> {

	public static final String SQLPOSTGRESQL_SELECT_FOR_ORDTRANSFER = "SELECT"
			+ " ID_ORD_TRANSFER as IdOrdTransfer, NRO_RECEPCIONES as nroRecepciones,"
			+ " COD_TIPO_ORDEN_EORDER as codTipoTDC"
			+ "	FROM schscom.eor_ord_transfer"
			+ "	WHERE cod_estado_orden = ?"
			+ "	AND COD_TIPO_ORDEN_LEGACY = 'OCNX'"
			+ "	AND NRO_ORDEN_LEGACY = ?";
	
	@Override
	public EorOrdTransferDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Long idOrdTransfer = rs.getLong("idOrdTransfer");
		Long nroRecepciones = rs.getLong("nroRecepciones");
		String codTipoTDC = rs.getString("codTipoTDC");
		return new EorOrdTransferDTO(idOrdTransfer, nroRecepciones, codTipoTDC);
	}
	
	
}

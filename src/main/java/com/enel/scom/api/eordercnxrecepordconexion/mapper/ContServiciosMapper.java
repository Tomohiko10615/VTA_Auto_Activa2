package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ContServiciosMapper implements RowMapper<Integer> {

	public static final String SQLORACLE_SELECT_FOR_CONTSERVICIOS = "SELECT"
			+ " COUNT(1) as contServicios"
			+ "	FROM ORD_ORDEN OVEN, VTA_SRV_VENTA SVEN, VTA_SOL_SRV_ELE SELE"
			+ "	WHERE OVEN.ID_ORDEN = SVEN.ID_ORD_VENTA"
			+ "	AND SVEN.ID_SOL_SRV_ELE = SELE.ID_SOL_SRV_ELE"
			+ "	AND OVEN.NRO_ORDEN = ?"
			+ "	AND OVEN.ID_TIPO_ORDEN = 5"
			+ "	AND OVEN.ID_EMPRESA = 3"
			+ "	AND SVEN.ESTADO NOT IN('Anulado','Eliminado')";

	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Integer contServicios = rs.getInt("contServicios");
		return contServicios;
	}
			
}

package com.enel.scom.api.eordercnxrecepordconexion.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.enel.scom.api.eordercnxrecepordconexion.dto.RegistroOrdenDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LectorPlano {

	private LectorPlano() {
		super();
	}

	public static List<RegistroOrdenDTO> obtenerOrdenes(String archivoPlano, Optional<String> origen) {
		List<RegistroOrdenDTO> registros = new ArrayList<>();
		List<String> lineas = leerLineas(archivoPlano);
		for (String linea : lineas) {
			registros.add(obtenerRegistro(linea, origen));
		}
		return registros;
	}

	private static List<String> leerLineas(String archivoPlano) {
		List<String> lineas = new ArrayList<>();
		FileReader reader = null;
		try {
			reader = new FileReader(archivoPlano);
		} catch (IOException e) {
			log.error("Error: No se encontró el archivo");
			e.printStackTrace();
			System.exit(1);
		}
		try (BufferedReader archivoPlanoReader = new BufferedReader(reader)) {
			String linea = archivoPlanoReader.readLine();
			while(linea != null) {
				lineas.add(linea);
				linea = archivoPlanoReader.readLine();
			}
		} catch (IOException e) {
			log.error("Error: No se pudo leer el archivo");
			e.printStackTrace();
		}
		return lineas;
	}
	
	private static RegistroOrdenDTO obtenerRegistro(String linea, Optional<String> origen) {
		List<String> campos = Arrays.asList((linea).split("\t", -1));
		String ptoEntrega = campos.get(2);
		if (ptoEntrega.charAt(5) == '�') {
			log.info("Valor anterior: {}", ptoEntrega);
			ptoEntrega = ptoEntrega.substring(0, 5) + 'é' + ptoEntrega.substring(6);
			log.info("Valor modificado: {}", ptoEntrega);
		}
		RegistroOrdenDTO registroOrden = RegistroOrdenDTO.builder()
				.numOrden(campos.get(0).trim())
				.numCuenta(campos.get(1).trim())
				.ptoEntrega(ptoEntrega.trim())
				.propEmpalme(campos.get(3).trim())
				.tipoConductor(campos.get(4).trim())
				.capInterruptor(campos.get(5).trim())
				.sisProteccion(campos.get(6).trim())
				.cajaMedicion(campos.get(7).trim())
				.cajaToma(campos.get(8).trim())
				.acometida(campos.get(9).trim())
				.sumDerecho(campos.get(10).trim())
				.sumIzquierdo(campos.get(11).trim())
				.numPcr(campos.get(12).trim())
				.tensionPcr(campos.get(13).trim())
				.sumPcrRef(campos.get(14).trim())
				.numMedidorInstall(campos.get(15).trim())
				.codMarcaInstall(campos.get(16).trim())
				.codModeloInstall(campos.get(17).trim())
				.fecLecturaInstall(campos.get(18).trim())
				.novedadInstall(campos.get(19).trim())
				.lecturaInstall(campos.get(20).trim())
				.numMedidorRet(campos.get(21).trim())
				.codMarcaRet(campos.get(22).trim())
				.codModeloRet(campos.get(23).trim())
				.fecLecturaRet(campos.get(24).trim())
				.novedadRet(campos.get(25).trim())
				.lecturaRet(campos.get(26).trim())
				.set(campos.get(27).trim())
				.alimentador(campos.get(28).trim())
				.sed(campos.get(29).trim())
				.llave(campos.get(30).trim())
				.sector(campos.get(31).trim())
				.zona(campos.get(32).trim())
				.correlativo(campos.get(33).trim())
				.build();
		
		// R.I. Agregado 01/11/2023 INICIO
		// Este campo es opcional y sólo viene en el flujo ODT
		if (!origen.isPresent() && campos.size() > 34) {
			registroOrden.setCodFactor(campos.get(34).trim());
		}
		// R.I. Agregado 01/11/2023 FIN
		
		if (origen.isPresent()) {
			registroOrden.setFechaEjecucion(campos.get(34).trim());
			registroOrden.setLatitudMedidor(campos.get(35).trim());
			registroOrden.setLongitudMedidor(campos.get(36).trim());
			registroOrden.setLatitudEmpalme(campos.get(37).trim());
			registroOrden.setLongitudEmpalme(campos.get(38).trim());
		}
		return registroOrden;	
	}

}

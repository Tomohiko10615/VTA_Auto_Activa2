package com.enel.scom.api.eordercnxrecepordconexion.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.scom.api.eordercnxrecepordconexion.dto.CaeTmpClienteAlterDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.CaeTmpClienteDTO;
import com.enel.scom.api.eordercnxrecepordconexion.dto.OrdenVentaDTO;

public class CaeTmpClienteAlterMapper implements RowMapper<CaeTmpClienteAlterDTO> {
	public static final String SQLORACLE_SELECT_FOR_CAETMPCLIENTEALTER = "select ww.ID_STATE, se.FEC_ACTIVACION\r\n"
			+ " from nuc_cuenta nc,\r\n"
			+ "     nuc_servicio ns,\r\n"
			+ "     srv_electrico se,\r\n"
			+ "     wkf_workflow ww\r\n"
			+ " where nc.id_cuenta = ns.id_cuenta\r\n"
			+ " and ns.id_servicio = se.id_servicio\r\n"
			+ " and se.ID_WORKFLOW = ww.ID_WORKFLOW\r\n"
			+ " and ww.PARENT_TYPE = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico'\r\n"
			+ " and nc.NRO_CUENTA = ?";

	@Override
	public CaeTmpClienteAlterDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	
		String ID_STATE = rs.getString("ID_STATE");
		String FEC_ACTIVACION = rs.getString("FEC_ACTIVACION");
		
		return new CaeTmpClienteAlterDTO(ID_STATE, FEC_ACTIVACION);
	}
}

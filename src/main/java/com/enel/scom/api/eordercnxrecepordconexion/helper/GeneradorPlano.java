package com.enel.scom.api.eordercnxrecepordconexion.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.auth.StaticUserAuthenticator;
import org.apache.commons.vfs2.impl.DefaultFileSystemConfigBuilder;
import org.apache.commons.vfs2.impl.DefaultFileSystemManager;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.enel.scom.api.eordercnxrecepordconexion.helper.dto.ParametrosEntrada;

import lombok.extern.slf4j.Slf4j;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.sftp.SFTPException;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

@Slf4j
public class GeneradorPlano {

	private GeneradorPlano() {
		super();
	}
	
	private static final Path ROOT_PATH = FileSystems.getDefault().getRootDirectories().iterator().next();
	private static final String PREFIJO_NOMBRE_ARCHIVO = "Res_AutoActivar_";
	private static final String EXTENSION = ".xls";
	private static final int ERROR_STATUS = 1;
	private static final String ORIGEN_EORDER = "EORDER";
	private static final String ORIGEN_AUTOACTIVA = "AUTOACTIVA";
	
	private static String origen;
	private static String planoSalida;
	private static String nombreArchivo;

	public static void generar(ParametrosEntrada parametrosEntrada, String fechaActual, String pathLog) {
		origen = parametrosEntrada.getOrigen().orElse(ORIGEN_AUTOACTIVA);
		if (origen.equals(ORIGEN_EORDER)) {
			log.info("*****ORIGEN EORDER*****");
			crearArchivo(pathLog, fechaActual);
		} else {
			log.info("*****ORIGEN AUTOACTIVA*****");
			crearArchivo(pathLog, parametrosEntrada.getNroODT());
		}
		escribirLinea("\n\nResultados de la AutoActivacion de las Ordenes de Venta\n");
		escribirLinea("======================================================\n\n");
	}

	private static void crearArchivo(String pathLog, String sufix) {
		nombreArchivo = PREFIJO_NOMBRE_ARCHIVO + sufix + EXTENSION;
		planoSalida = ROOT_PATH + pathLog + nombreArchivo;
		Path planoSalidaPath = Paths.get(planoSalida);
	    try {
			Files.createFile(planoSalidaPath);
	    } catch (FileAlreadyExistsException e) {
	    	log.info("*****EL ARCHIVO YA EXISTE*****");
			if (origen.equals(ORIGEN_AUTOACTIVA)) {
				log.error("1 archivo por cada ejecucion - Ejecucion independiente de proceso de autoactivacion");
				log.error("No se puede reemplazar el archivo");
				throw new UncheckedIOException(e);
			} else {
				log.info("1 archivo por dia - Ejecución de proceso desde proceso de recepcion de Ordenes de Nuevas conexiones");
				log.info("Se escribira al final del archivo...");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(ERROR_STATUS);
		}
	}
	
	public static void escribirLinea(String linea) {
		try(PrintWriter writer = 
				new PrintWriter(
						new FileWriter(planoSalida, true))) {
			log.info(linea);
			writer.write(linea);
		} catch (IOException e) {
			log.error("*****ERROR AL ESCRIBIR EL ARCHIVO*****");
			log.error("Linea: {}", linea);
			e.printStackTrace();
			System.exit(ERROR_STATUS);
		}
	}

	public static void subirAServidorFTP() {
		String server = System.getenv("FTP_SERVER");
        int port = 11022;
        //String remoteFilePath = "/PRE/edelnor/repodtgrids/conexiones/" + nombreArchivo;
        String remoteFilePath = System.getenv("REMOTE_PATH") + nombreArchivo;
        String localFilePath = planoSalida;
        
        String ftpUser = System.getenv("FTP_USER") + System.getenv("FTP_ENV");
        String ftpPassword = System.getenv("FTP_PASSWORD");
        log.info(ftpUser);
        log.info(ftpPassword);
        
        JSch jsch = new JSch();
        Session session;
		try {
			// Crear una sesión de SFTP y conectarse al servidor
			
			session = jsch.getSession(ftpUser, server, port);
			session.setPassword(ftpPassword);
			log.info(session.getHost());
			log.info("{}", session.getPort());
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no"); //SE COLOCO LINEA FALTANTE 08082023 JAAG
	      //  config.put("cipher.s2c", "aes256-ctr"); //SECOMENTA 08082023 JAAG
	      //  config.put("cipher.c2s", "aes256-ctr"); //SECOMENTA 08082023 JAAG
	        session.setConfig(config);
	        session.connect();
	        log.info("here");
	        ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
	        sftpChannel.connect();
	        sftpChannel.put(localFilePath, remoteFilePath);
	        sftpChannel.disconnect();
	        session.disconnect();
		} catch (JSchException | SftpException e) {
			e.printStackTrace();
			log.error("Error al subir archivo de resultados al servidor SFTP");
		}
		
		
		String sftpUrl = "sftp://" + ftpUser + ":" + ftpPassword + "@" + server + ":" + port;
		String filePath = "/PRE/edelnor/repodtgrids/conexiones";
		
		/*
		try {
			FileSystemManager manager = VFS.getManager();
			 
		    FileObject local = manager.resolveFile(
		      System.getProperty("user.dir") + "/" + localFilePath);
		    FileObject remote;
			remote = manager.resolveFile(
			  "sftp://" + ftpUser + ":" + ftpPassword + "@" + server + ":" + port + "/" + remoteFilePath);
			remote.copyFrom(local, Selectors.SELECT_SELF);
			local.close();
		    remote.close();
		} catch (FileSystemException e) {
			e.printStackTrace();
		}
		*/
		
		/*
		SSHClient ssh = new SSHClient();
		ssh.addHostKeyVerifier(new PromiscuousVerifier());
		try {
		    ssh.setConnectTimeout(20000);
		    ssh.connect("10.154.70.227");
		    ssh.authPassword(ftpUser, ftpPassword);
		    
		    // Configurar opciones de SFTP
		    SFTPClient sftp = ssh.newSFTPClient();
		    sftp.setVersion(SFTPv3Client.VERSION_3);
		    
		    // Configurar opciones de SSH
		    ssh.useCompression(false);
		    ssh.setAlgorithms(Arrays.asList("aes256-ctr"));
		    
		    File localFile = new File(localFilePath);
		    String remotePath = filePath;
		    sftp.put(localFile.getAbsolutePath(), remotePath);
		    sftp.close();
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    try {
		        ssh.disconnect();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		}
		*/
		
		/*
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(ftpUser, server, port);
            session.setPassword(ftpPassword);
            
            // Set SFTP protocol and cryptographic protocol
            session.setConfig("PreferredAuthentications", "publickey,password");
            session.setConfig("cipher.s2c", "aes256-ctr");
            session.setConfig("cipher.c2s", "aes256-ctr");
            
            // Set SSH implementation
            session.setConfig("StrictHostKeyChecking", "no");
            session.setConfig("compression.s2c", "none");
            session.setConfig("compression.c2s", "none");
            session.setConfig("server_host_key", "ssh-rsa,ssh-dss");
            session.setConfig("ServerAliveInterval", "60");
            session.setConfig("ServerAliveCountMax", "3");
            
            session.connect();
            
            ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            
            // Disable compression
            channel.setFilenameEncoding("UTF-8");
            channel.put(localFilePath, remoteFilePath);
            
            channel.disconnect();
            session.disconnect();
            
            System.out.println("File uploaded successfully.");
         } catch (JSchException | SftpException e) {
            e.printStackTrace();
         }
         */
	 
	    
	}
}

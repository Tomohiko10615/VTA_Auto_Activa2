package com.enel.scom.api.eordercnxrecepordconexion.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DatosCorrelativoMoverDTO {
    private long idRuta;
    private String codRuta;
    private long idServVenta;
    private int  idDisponible;
    private long idSector;
    private long idZona;
    private String correl;
}
